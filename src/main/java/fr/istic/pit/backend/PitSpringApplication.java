package fr.istic.pit.backend;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Point d'entrée de l'application Spring Boot
 */
@SpringBootApplication
public class PitSpringApplication {

    public static void main(String[] args) {
        SpringApplication.run(PitSpringApplication.class);
    }

}
