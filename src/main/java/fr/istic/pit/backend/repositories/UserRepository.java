package fr.istic.pit.backend.repositories;

import fr.istic.pit.backend.model.entities.User;

/**
 * Repository des utilisateurs
 */
public interface UserRepository extends EntityRepository<User, Long> {
    /**
     * Récupère les informations d'un utilisateur grâce à son login
     *
     * @param login le login de l'utilisateur
     * @return l'utilisateur trouvé
     */
    User findByLogin(String login);
}
