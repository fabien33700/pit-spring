package fr.istic.pit.backend.repositories;

import fr.istic.pit.backend.model.entities.PointOfInterest;

/**
 * Repository pour les points d'intérêts
 */
public interface PointOfInterestRepository extends EntityRepository<PointOfInterest, Long> {
}
