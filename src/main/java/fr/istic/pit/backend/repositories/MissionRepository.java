package fr.istic.pit.backend.repositories;

import fr.istic.pit.backend.model.entities.Mission;

/**
 * Repository pour les missions drone
 */
public interface MissionRepository extends EntityRepository<Mission, Long> {
}
