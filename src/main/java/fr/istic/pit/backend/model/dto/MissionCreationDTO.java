package fr.istic.pit.backend.model.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import io.swagger.v3.oas.annotations.media.Schema;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.ArrayList;
import java.util.Collection;

import static com.fasterxml.jackson.annotation.JsonInclude.Include.NON_NULL;

/**
 * Une mission est une suite de points géographiques
 * suivis par le drone.
 * DTO servant à la création de la mission.
 */
@Valid
@JsonInclude(NON_NULL)
@Schema(
    name = "Création de mission",
    description = "Les informations nécessaires à la création d'une mission pour le drone, constituée d'une séquence de points"
)
public class MissionCreationDTO {
    /**
     * La liste des points de la mission
     */
    @NotNull
    @Size(min = 2)
    @Schema(description = "La liste des points constituant la mission")
    public final Collection<DronePointDTO> points = new ArrayList<>();

    /**
     * Indique si la mission se joue en boucle (true) ou en aller-retour
     */
    @NotNull
    @Schema(description = "Le mode de déroulé de la mission (true = boucle, false = A/R)", type = "boolean")
    public boolean missionLoop;
}
