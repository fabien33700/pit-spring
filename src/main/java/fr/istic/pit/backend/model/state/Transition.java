package fr.istic.pit.backend.model.state;

import java.util.function.Function;

/**
 * Interface fonctionnelle décrivant une transition d'un état S à un autre état
 * (correspond aux méthodes de transitions de la classe S)
 *
 * @param <S> Le type d'état de départ
 */
@FunctionalInterface
public interface Transition<S extends MacroState.State> extends Function<S, MacroState.State> {
}
