package fr.istic.pit.backend.model.dto;

import io.swagger.v3.oas.annotations.media.Schema;

import javax.validation.Valid;
import javax.validation.constraints.Max;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.PositiveOrZero;

/**
 * DTO stockant le nouveau rôle à attribuer à un moyen
 */
@Valid
@Schema(
    name = "Changement de rôle",
    description = "Un objet contenant le nouveau rôle"
)
public class RoleChangeDTO {
    /**
     * Le rôle à attribuer
     */
    @Schema(description = "Le rôle du point d'intérêt. \n **Code couleurs** : \n" +
        "<table>" +
        "    <thead>" +
        "        <tr>" +
        "            <th>Code</th>" +
        "            <th>Couleur</th>" +
        "            <th>Signification</th>" +
        "        </tr>" +
        "    </thead>" +
        "    <tbody>" +
        "        <tr>" +
        "            <td>0</td>" +
        "            <td>noir</td>" +
        "            <td>Cheminement (accès, rocades, pénétrantes). <br/>Couleur de remplacement par défaut</td>" +
        "        </tr>" +
        "        <tr>" +
        "            <td>1</td>" +
        "            <td>rouge</td>" +
        "            <td>Lié à l’incendie</td>" +
        "        </tr>" +
        "        <tr>" +
        "            <td>2</td>" +
        "            <td>orange</td>" +
        "            <td>Lié aux « risques particuliers »</td>" +
        "        </tr>" +
        "        <tr>" +
        "            <td>3</td>" +
        "            <td>vert</td>" +
        "            <td>Lié aux personnes</td>" +
        "        </tr>" +
        "        <tr>" +
        "            <td>4</td>" +
        "            <td>bleu</td>" +
        "            <td>Lié à l’eau</td>" +
        "        </tr>" +
        "        <tr>" +
        "            <td>5</td>" +
        "            <td>violet</td>" +
        "            <td>Lié au commandement, à la sectorisation</td>" +
        "        </tr>" +
        "    </tbody>" +
        "</table>")
    @NotNull
    @PositiveOrZero
    @Max(5)
    public Integer role;
}