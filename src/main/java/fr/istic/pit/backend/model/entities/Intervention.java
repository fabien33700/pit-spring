package fr.istic.pit.backend.model.entities;

import fr.istic.pit.backend.model.enums.DroneAvailability;
import org.hibernate.annotations.Where;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collection;

import static java.util.Objects.requireNonNull;
import static javax.persistence.GenerationType.IDENTITY;

/**
 * Entité stockant les interventions
 */
@Entity
@Where(clause = "active = true")
public class Intervention {

    /*
     * Constantes codes sinistre
     */
    public static final String FDF = "FDF";
    public static final String FAN = "FAN";
    public static final String FHA = "FHA";

    public static final String ALP = "ALP";

    /**
     * Identifiant technique, base de données
     */
    @GeneratedValue(strategy = IDENTITY)
    @Id
    private Long id;

    /**
     * Identifiant métier unique de l'intervention
     */
    private String name;

    /**
     * Code sinistre
     */
    private String sinisterCode;

    /**
     * Heure de début d'intervention
     */
    private LocalDateTime startTime;

    /**
     * Heure de fin d'intervention
     */
    private LocalDateTime endTime;

    /**
     * Latitude
     */
    private Double latitude;

    /**
     * Longitude
     */
    private Double longitude;

    /**
     * Informations complémentaires
     */
    private String informations;

    /**
     * Liste des points d'intérêts pour l'intervention
     */
    @OneToMany(mappedBy = "intervention", cascade = CascadeType.ALL)
    @OrderBy("id asc")
    @Where(clause = "active = true")
    private final Collection<PointOfInterest> pointsOfInterest = new ArrayList<>();

    /**
     * Liste des moyens pour l'intervention
     */
    @OneToMany(mappedBy = "intervention", cascade = CascadeType.ALL)
    @OrderBy("id asc")
    @Where(clause = "active = true")
    private final Collection<Resource> resources = new ArrayList<>();

    /**
     * État d'un éventuel drone
     */
    @Enumerated(EnumType.STRING)
    private DroneAvailability droneAvailability;

    /**
     * La mission en cours
     */
    @ManyToOne
    private Mission currentMission;

    /**
     * Si l'intervention est active ou non
     */
    @Column(nullable = false)
    private boolean active = true;

    public Intervention() {
    }

    private Intervention(Builder builder) {
        setId(builder.id);
        setName(builder.name);
        setSinisterCode(builder.sinisterCode);
        setStartTime(builder.startTime);
        setEndTime(builder.endTime);
        setLatitude(builder.latitude);
        setLongitude(builder.longitude);
        setInformations(builder.informations);
        setDroneAvailability(builder.droneAvailability);
        setCurrentMission(builder.currentMission);
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSinisterCode() {
        return sinisterCode;
    }

    public void setSinisterCode(String sinisterCode) {
        this.sinisterCode = sinisterCode;
    }

    public LocalDateTime getStartTime() {
        return startTime;
    }

    public void setStartTime(LocalDateTime startTime) {
        this.startTime = startTime;
    }

    public LocalDateTime getEndTime() {
        return endTime;
    }

    public void setEndTime(LocalDateTime endTime) {
        this.endTime = endTime;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public String getInformations() {
        return informations;
    }

    public void setInformations(String informations) {
        this.informations = informations;
    }

    public Collection<Resource> getResources() {
        return new ArrayList<>(resources);
    }

    public void addResource(Resource resource) {
        requireNonNull(resource);
        if (resources.contains(resource))
            return;

        resources.add(resource);
        resource.setIntervention(this);
    }

    public void removeResource(Resource resource) {
        if (!resources.contains(resource))
            return;

        resources.remove(resource);
        resource.setIntervention(null);
    }

    public Collection<PointOfInterest> getPointsOfInterest() {
        return new ArrayList<>(pointsOfInterest);
    }

    public void addPointOfInterest(PointOfInterest pointOfInterest) {
        requireNonNull(pointOfInterest);
        if (pointsOfInterest.contains(pointOfInterest))
            return;

        pointsOfInterest.add(pointOfInterest);
        pointOfInterest.setIntervention(this);
    }

    public void removePointOfInterest(PointOfInterest pointOfInterest) {
        if (!pointsOfInterest.contains(pointOfInterest))
            return;

        pointsOfInterest.remove(pointOfInterest);
        pointOfInterest.setIntervention(null);
    }

    public DroneAvailability getDroneAvailability() {
        return droneAvailability;
    }

    public void setDroneAvailability(DroneAvailability droneAvailability) {
        this.droneAvailability = droneAvailability;
    }

    public Mission getCurrentMission() {
        return currentMission;
    }

    public void setCurrentMission(Mission currentMission) {
        this.currentMission = currentMission;
    }

    /**
     * Builder
     */
    public static final class Builder {
        private Long id;
        private String name;
        private String sinisterCode;
        private LocalDateTime startTime;
        private LocalDateTime endTime;
        private Double latitude;
        private Double longitude;
        private String informations;
        private DroneAvailability droneAvailability;
        private Mission currentMission;

        public Builder() {
        }

        public Builder id(Long val) {
            id = val;
            return this;
        }

        public Builder name(String val) {
            name = val;
            return this;
        }

        public Builder sinisterCode(String val) {
            sinisterCode = val;
            return this;
        }

        public Builder startTime(LocalDateTime val) {
            startTime = val;
            return this;
        }

        public Builder endTime(LocalDateTime val) {
            endTime = val;
            return this;
        }

        public Builder latitude(Double val) {
            latitude = val;
            return this;
        }

        public Builder longitude(Double val) {
            longitude = val;
            return this;
        }

        public Builder informations(String val) {
            informations = val;
            return this;
        }

        public Builder droneAvailability(DroneAvailability val) {
            droneAvailability = val;
            return this;
        }

        public Builder currentMission(Mission val) {
            currentMission = val;
            return this;
        }

        public Intervention build() {
            return new Intervention(this);
        }
    }

    /**
     * Fusionne les propriétés de l'intervention source dans l'objet courant.
     *
     * @param source L'objet source
     */
    public void merge(Intervention source) {
        requireNonNull(source, "Source intervention object is null");
        setName(source.name);
        setSinisterCode(source.sinisterCode);
        setStartTime(source.startTime);
        setEndTime(source.endTime);
        setLatitude(source.latitude);
        setLongitude(source.longitude);
        setInformations(source.informations);
        setDroneAvailability(source.droneAvailability);
        setCurrentMission(source.currentMission);
    }
}
