package fr.istic.pit.backend.model.enums;

/**
 * Énumération pour la disponibilité du drone sur l'intervention
 */
public enum DroneAvailability {
    /**
     * Pas de drone sur l'intervention
     */
    NONE,

    /**
     * Le drone n'est pas disponible (libéré ou en atterrissage d'urgence)
     */
    UNAVAILABLE,

    /**
     * Drone disponible sur l'intervention
     */
    AVAILABLE
}
