package fr.istic.pit.backend.model.entities;

import org.hibernate.annotations.Where;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import java.util.Objects;

import static java.util.Objects.requireNonNull;

/**
 * Entité stockant un point d'intérêt
 */
@Entity
@DiscriminatorValue("POI")
@Where(clause = "active = true")
public class PointOfInterest extends AbstractMapPoint {

    public PointOfInterest() {
    }

    private PointOfInterest(Builder builder) {
        setLatitude(builder.latitude);
        setLongitude(builder.longitude);
        setRole(builder.role);
        setPictoType(builder.pointType);
        setIntervention(builder.intervention);
    }

    public void setIntervention(Intervention newInterv) {
        if (Objects.equals(intervention, newInterv))
            return;

        Intervention oldInterv = intervention;
        this.intervention = newInterv;

        if (oldInterv != null)
            oldInterv.removePointOfInterest(this);

        if (newInterv != null)
            newInterv.addPointOfInterest(this);
    }

    public static final class Builder {
        private Double latitude;
        private Double longitude;
        private Integer role;
        private String pointType;
        private Intervention intervention;

        public Builder() {
        }

        public Builder latitude(Double val) {
            latitude = val;
            return this;
        }

        public Builder longitude(Double val) {
            longitude = val;
            return this;
        }

        public Builder role(Integer val) {
            role = val;
            return this;
        }

        public Builder pointType(String val) {
            pointType = val;
            return this;
        }

        public Builder intervention(Intervention val) {
            intervention = val;
            return this;
        }

        public PointOfInterest build() {
            return new PointOfInterest(this);
        }
    }

    /**
     * Fusionne les propriétés du point d'intérêt source dans l'objet courant.
     *
     * @param source L'objet source
     */
    public void merge(PointOfInterest source) {
        requireNonNull(source, "Source point of interest object is null");
        setPictoType(source.pictoType);
        setLatitude(source.latitude);
        setLongitude(source.longitude);
        setRole(source.role);
    }
}
