package fr.istic.pit.backend.model.entities;

import org.hibernate.annotations.DiscriminatorOptions;

import javax.persistence.*;

import static javax.persistence.GenerationType.IDENTITY;

/**
 * Entité abstraite stockant les informations communes pour les points à stocker
 * sur la carte d'une intervention.
 */
@Entity
@Inheritance
@Table(name = "map_points")
@DiscriminatorOptions(force = true)
@DiscriminatorColumn(name = "map_point_type")
public abstract class AbstractMapPoint {
    /**
     * Identifiant technique
     */
    @GeneratedValue(strategy = IDENTITY)
    @Id
    private long id;

    /**
     * L'intervention est-elle active ?
     */
    @Column(nullable = false)
    protected boolean active = true;

    /**
     * La latitude du point de l'intervention
     */
    protected Double latitude;

    /**
     * La longitude du point de l'intervention
     */
    protected Double longitude;

    /**
     * Rôle du point
     */
    protected Integer role;

    /**
     * Type de pictogramme (POI) / Type de moyen
     */
    protected String pictoType;

    /**
     * L'intervention à laquelle est rattachée le point
     */
    @ManyToOne
    protected Intervention intervention;

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    @Transient
    public boolean isInCrm() {
        return latitude == null || longitude == null ||
            latitude == 0.0d || longitude == 0.0d;
    }

    public Integer getRole() {
        return role;
    }

    public void setRole(Integer role) {
        this.role = role;
    }

    public String getPictoType() {
        return pictoType;
    }

    public void setPictoType(String pointType) {
        this.pictoType = pointType;
    }

    public Intervention getIntervention() {
        return intervention;
    }
}
