package fr.istic.pit.backend.model.state;

/**
 * Classe statique modélisant la machine à état micro pour la situation des moyens
 * sur la carte de l'intervention.
 */
public final class MicroState {
    /**
     * En trajet
     */
    public static final String MOVING = "MOVING";

    /**
     * Sur place
     */
    public static final String IDLE = "IDLE";

    /**
     * État final
     */
    public static final String END = "END";

    MicroState() {
    }
}
