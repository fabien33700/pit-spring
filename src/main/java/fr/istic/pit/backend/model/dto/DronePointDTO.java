package fr.istic.pit.backend.model.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import io.swagger.v3.oas.annotations.media.Schema;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import static com.fasterxml.jackson.annotation.JsonInclude.Include.NON_NULL;
import static io.swagger.v3.oas.annotations.media.Schema.AccessMode.READ_ONLY;

/**
 * DTO représentant un point sur lequel se rend le drone.
 */
@Valid
@JsonInclude(NON_NULL)
@Schema(
    name = "Point de drone",
    description = "Représente un point sur lequel peut se rendre un drone"
)
public class DronePointDTO {
    /**
     * Identifiant technique du point
     */
    @Schema(description = "Identifiant technique du point", accessMode = READ_ONLY)
    public Long id;

    /**
     * La mission à laquelle se rapporte le point
     */
    @Schema(
        description = "L'identifiant technique de la mission auquel se rattache le point",
        type = "number",
        accessMode = READ_ONLY
    )
    public Long missionId;

    /**
     * Numéro d'ordre du point dans la mission
     */
    @Schema(
        description = "Le numéro d'ordre du point dans le déroulé de la mission",
        type = "number",
        accessMode = READ_ONLY
    )
    public int orderNumber;

    /**
     * Latitude du point
     */
    @Schema(description = "La latitude du point, au format décimal", type = "number")
    @NotNull
    public Double latitude;

    /**
     * Longitude du point
     */
    @Schema(description = "La longitude du point, au format décimal", type = "number")
    @NotNull
    public Double longitude;

    /**
     * Altitude du point (relative au niveau de la mer)
     */
    @Schema(description = "L'altitude du point, relative au niveau de la mer, au format décimal", type = "number")
    public Double altitude;
}
