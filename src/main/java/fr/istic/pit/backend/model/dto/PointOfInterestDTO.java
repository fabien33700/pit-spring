package fr.istic.pit.backend.model.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import io.swagger.v3.oas.annotations.media.Schema;

import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import static com.fasterxml.jackson.annotation.JsonInclude.Include.NON_NULL;
import static io.swagger.v3.oas.annotations.media.Schema.AccessMode.READ_ONLY;

/**
 * DTO représentant les points d'intérêts
 */
@Valid
@Schema(
    name = "Point d'intérêt (POI)",
    description = "La resource modélisant un point d'intérêt (POI). Les POI sont généralement fixes et transmis par le SIG."
)
@JsonInclude(NON_NULL)
public class PointOfInterestDTO {

    @Schema(description = "Identifiant technique de la ressource", accessMode = READ_ONLY)
    public Long id;

    /**
     * Latitude
     */
    @Schema(description = "La latitude du point d'intérêt, au format décimal", type = "number")
    @NotNull
    public Double latitude;

    /**
     * Longitude
     */
    @Schema(description = "La longitude du point d'intérêt, au format décimal", type = "number")
    @NotNull
    public Double longitude;

    /**
     * Un code identifiant le type de pictogramme à utiliser
     */
    @NotEmpty
    @Schema(description = "Un code identifiant le type de pictogramme à utiliser")
    public String pictoType;

    /**
     * L'id de l'intervention
     */
    @Schema(description = "L'identifiant technique de l'intervention",
        accessMode = READ_ONLY, type = "number")
    public long intervId;
}
