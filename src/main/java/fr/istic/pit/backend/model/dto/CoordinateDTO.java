package fr.istic.pit.backend.model.dto;

import io.swagger.v3.oas.annotations.media.Schema;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

/**
 * DTO représentant des coordonnées GPS, utilisé pour le déplacement de moyen
 */
@Valid
@Schema(
    name = "Coordonnées",
    description = "Des coordonnées GPS (latitude, longitude)"
)
public class CoordinateDTO {
    /**
     * Latitude
     */
    @Schema(description = "La latitude du point d'intérêt, au format décimal", type = "number")
    @NotNull
    public Double latitude;

    /**
     * Longitude
     */
    @Schema(description = "La longitude du point d'intérêt, au format décimal", type = "number")
    @NotNull
    public Double longitude;
}
