package fr.istic.pit.backend.model.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import io.swagger.v3.oas.annotations.media.Schema;

import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collection;

import static com.fasterxml.jackson.annotation.JsonInclude.Include.NON_EMPTY;
import static io.swagger.v3.oas.annotations.media.Schema.AccessMode.READ_ONLY;

/**
 * DTO représentant une intervention
 */
@Valid
@JsonInclude(JsonInclude.Include.NON_NULL)
@Schema(
    name = "Intervention",
    description = "La resource modélisant une intervention"
)
public class InterventionDTO {
    @Schema(description = "Identifiant technique de la ressource", accessMode = READ_ONLY)
    public Long id;

    /**
     * Identifiant métier unique de l'intervention
     */
    @Schema(description = "Identifiant métier unique de l'intervention")
    @NotEmpty
    public String name;

    /**
     * Code sinistre
     */
    @Schema(description = "Code sinistre", allowableValues = {"INC", "SAP"})
    @NotEmpty
    public String sinisterCode;

    /**
     * Heure de début d'intervention
     */
    @Schema(description = "Heure de début d'intervention. *Automatiquement définie à la création.*", accessMode = READ_ONLY)
    public LocalDateTime startTime;

    /**
     * Heure de fin d'intervention
     */
    @Schema(description = "Heure de fin d'intervention. *Automatiquement définie à la fin de l'intervention.*", accessMode = READ_ONLY)
    public LocalDateTime endTime;

    /**
     * Latitude
     */
    @Schema(description = "La latitude du lieu de l'intervention, au format décimal", type = "number")
    @NotNull
    public Double latitude;

    /**
     * Longitude
     */
    @Schema(description = "La longitude du lieu de l'intervention, au format décimal", type = "number")
    @NotNull
    public Double longitude;

    /**
     * Informations complémentaires
     */
    @Schema(description = "Des informations complémentaires pour l'intervention")
    @JsonInclude(NON_EMPTY)
    public String informations;

    /**
     * Disponibilité du drone
     */
    @Schema(
        description = "La disponibilité du drone. <br/>" +
            "- **NONE** : Pas de drone déployé sur intervention<br/>" +
            "- **UNAVAILABLE** : Le drone est indisponible : soit il a été libéré, soit il est en atterrissage d'urgence<br/>" +
            "- **AVAILABLE** : Le drone est disponible",
        allowableValues = "NONE, UNAVAILABLE, AVAILABLE",
        accessMode = READ_ONLY
    )
    public String droneAvailability;

    /**
     * Liste des moyens pour l'intervention
     */
    @Schema(description = "La liste des moyens à mobiliser au départ de l'intervention.")
    @Valid
    @JsonInclude(NON_EMPTY)
    public final Collection<ResourceDTO> resources = new ArrayList<>();
}
