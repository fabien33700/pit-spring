package fr.istic.pit.backend.model.entities;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import java.time.LocalDateTime;
import java.util.Objects;

import static java.util.Objects.requireNonNull;

/**
 * Entité stockant les moyens
 */
@Entity
@DiscriminatorValue("Resource")
public class Resource extends AbstractMapPoint {

    /*
     * Constantes types de moyens
     */
    public static final String VLCG = "VLCG";

    public static final String FPT = "FPT";

    public static final String VSAV = "VSAV";

    /*
     * Constantes rôles
     */
    public static final int DEFAUT = 0;

    public static final int INCENDIE = 1;

    public static final int RISQUES = 2;

    public static final int PERSONNES = 3;

    public static final int EAU = 4;

    public static final int COMMANDEMENT = 5;

    /**
     * Le nom/identifiant du moyen
     */
    private String name;

    /**
     * L'heure de la demande
     */
    private LocalDateTime requestTime;

    /**
     * L'heure de départ
     */
    private LocalDateTime startingTime;

    /**
     * L'heure d'arrivée
     */
    private LocalDateTime arrivalTime;

    /**
     * L'heure de libération
     */
    private LocalDateTime releaseTime;

    /**
     * État macro du moyen
     */
    private String macroState;

    /**
     * État micro du moyen
     * (en trajet, sur place)
     */
    private String microState;

    public Resource() {
    }

    private Resource(Builder builder) {
        setLatitude(builder.latitude);
        setLongitude(builder.longitude);
        setRole(builder.role);
        setPictoType(builder.pointType);
        setIntervention(builder.intervention);
        setName(builder.name);
        setRequestTime(builder.requestTime);
        setStartingTime(builder.startingTime);
        setArrivalTime(builder.arrivalTime);
        setReleaseTime(builder.releaseTime);
        setMacroState(builder.macroState);
        setMicroState(builder.microState);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public LocalDateTime getRequestTime() {
        return requestTime;
    }

    public void setRequestTime(LocalDateTime requestTime) {
        this.requestTime = requestTime;
    }

    public LocalDateTime getStartingTime() {
        return startingTime;
    }

    public void setStartingTime(LocalDateTime startingTime) {
        this.startingTime = startingTime;
    }

    public LocalDateTime getArrivalTime() {
        return arrivalTime;
    }

    public void setArrivalTime(LocalDateTime arrivalTime) {
        this.arrivalTime = arrivalTime;
    }

    public LocalDateTime getReleaseTime() {
        return releaseTime;
    }

    public void setReleaseTime(LocalDateTime releaseTime) {
        this.releaseTime = releaseTime;
    }

    public String getMacroState() {
        return macroState;
    }

    public void setMacroState(String macroState) {
        this.macroState = macroState;
    }

    public String getMicroState() {
        return microState;
    }

    public void setMicroState(String microState) {
        this.microState = microState;
    }

    public void setIntervention(Intervention newInterv) {
        if (Objects.equals(intervention, newInterv))
            return;

        Intervention oldInterv = intervention;
        this.intervention = newInterv;

        if (oldInterv != null)
            oldInterv.removeResource(this);

        if (newInterv != null)
            newInterv.addResource(this);
    }

    /**
     * Builder
     */
    public static final class Builder {
        private Double latitude;
        private Double longitude;
        private Integer role;
        private String pointType;
        private Intervention intervention;
        private String name;
        private LocalDateTime requestTime;
        private LocalDateTime startingTime;
        private LocalDateTime arrivalTime;
        private LocalDateTime releaseTime;
        private String macroState;
        private String microState;

        public Builder() {
        }

        public Builder latitude(Double val) {
            latitude = val;
            return this;
        }

        public Builder longitude(Double val) {
            longitude = val;
            return this;
        }

        public Builder role(Integer val) {
            role = val;
            return this;
        }

        public Builder pointType(String val) {
            pointType = val;
            return this;
        }

        public Builder intervention(Intervention val) {
            intervention = val;
            return this;
        }

        public Builder name(String val) {
            name = val;
            return this;
        }

        public Builder requestTime(LocalDateTime val) {
            requestTime = val;
            return this;
        }

        public Builder startingTime(LocalDateTime val) {
            startingTime = val;
            return this;
        }

        public Builder arrivalTime(LocalDateTime val) {
            arrivalTime = val;
            return this;
        }

        public Builder releaseTime(LocalDateTime val) {
            releaseTime = val;
            return this;
        }

        public Builder macroState(String val) {
            macroState = val;
            return this;
        }

        public Builder microState(String val) {
            microState = val;
            return this;
        }

        public Resource build() {
            return new Resource(this);
        }
    }

    /**
     * Fusionne les propriétés du moyen source dans l'objet courant.
     *
     * @param source L'objet source
     */
    public void merge(Resource source) {
        requireNonNull(source, "Source resource object is null");
        setLatitude(source.latitude);
        setLongitude(source.longitude);
        setRole(source.role);
        setPictoType(source.pictoType);
        setName(source.name);
        setRequestTime(source.requestTime);
        setStartingTime(source.startingTime);
        setArrivalTime(source.arrivalTime);
        setReleaseTime(source.releaseTime);
        setMacroState(source.macroState);
        setMicroState(source.microState);
    }
}
