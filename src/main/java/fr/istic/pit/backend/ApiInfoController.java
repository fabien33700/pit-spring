package fr.istic.pit.backend;

import fr.istic.pit.backend.services.mqtt.sync.RestInterceptionAspect;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import java.lang.management.ManagementFactory;
import java.lang.management.RuntimeMXBean;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.HashMap;
import java.util.Map;

import static fr.istic.pit.backend.config.OpenAPIConfig.API_VERSION_PROPERTY;
import static org.springframework.http.ResponseEntity.ok;

/**
 * Contrôleur pour la description de l'API
 */
@RestController("/")
public class ApiInfoController {

    private final String apiVersion;

    private final RuntimeMXBean rb;

    private final RestInterceptionAspect restInterceptionAspect;

    public ApiInfoController(@Value(API_VERSION_PROPERTY) String apiVersion,
                             RestInterceptionAspect restInterceptionAspect) {
        this.apiVersion = apiVersion;
        this.restInterceptionAspect = restInterceptionAspect;
        this.rb = ManagementFactory.getRuntimeMXBean();
    }

    @GetMapping
    public ResponseEntity<?> getApiInformation(UriComponentsBuilder builder) {
        String docUri = builder.path("/swagger-ui.html").toUriString();
        ZonedDateTime utcTime = ZonedDateTime.now(ZoneId.of("UTC"));

        Map<String, Object> info = new HashMap<>();
        info.put("version", apiVersion);
        info.put("name", "Pit Project Backend API");
        info.put("description", "The backend Web Service to communicate with all frontend " +
            "Android apps instances and with drones");
        info.put("documentation", docUri);
        info.put("uptime", rb.getUptime());
        info.put("actualTime", utcTime);
        info.put("requests", restInterceptionAspect.getRequestCount());

        return ok(info);
    }
}
