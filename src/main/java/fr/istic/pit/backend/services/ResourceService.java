package fr.istic.pit.backend.services;

import fr.istic.pit.backend.model.entities.Resource;
import fr.istic.pit.backend.repositories.ResourceRepository;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Services de données pour la recherche des moyens
 */
@Service
public class ResourceService {

    /**
     * Repository des moyens
     */
    private final ResourceRepository resourceRepository;

    /**
     * Constructeur du service.
     *
     * @param resourceRepository Repository des moyens
     */
    public ResourceService(ResourceRepository resourceRepository) {
        this.resourceRepository = resourceRepository;
    }

    /**
     * Récupérer les moyens en fonction des états recherchés.
     *
     * @param macro L'état macro des moyens à rechercher
     * @param micro L'état micro des moyens à rechercher
     * @return La liste des moyens trouvés
     */
    public List<Resource> getAllByState(String macro, String micro) {
        // Si pas de critères d'état, renvoyer tous les moyens
        if (macro == null && micro == null)
            return this.resourceRepository.findAll();

        // Recherche par état micro
        if (macro == null)
            return this.resourceRepository.findAllByMicroState(micro.toUpperCase());

        // Recherche par état macro
        if (micro == null)
            return this.resourceRepository.findAllByMacroState(macro.toUpperCase());

        // Recherche par état macro et micro
        return this.resourceRepository.findAllByMacroStateAndMicroState(
            macro.toUpperCase(), micro.toUpperCase());
    }
}
