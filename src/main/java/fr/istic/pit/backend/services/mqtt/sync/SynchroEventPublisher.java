package fr.istic.pit.backend.services.mqtt.sync;

import com.fasterxml.jackson.databind.ObjectMapper;
import fr.istic.pit.backend.services.mqtt.AbstractMqttService;
import fr.istic.pit.backend.services.mqtt.sync.model.UpdateEvent;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

/**
 * Service d'émission des messages MQTT vers le broker de messages.
 */
@Service
public class SynchroEventPublisher extends AbstractMqttService {

    /**
     * Topic de base par défaut
     */
    private static final String DEFAULT_MQTT_BASE_TOPIC = "pit/sync/events";

    /**
     * Placeholder Spring pour récupérer la valeur de la propriété contenant le topic de base
     */
    private static final String MQTT_BASE_TOPIC_PROP = "${pit-backend.mqtt.base_topic.sync:" + DEFAULT_MQTT_BASE_TOPIC + "}";

    /**
     * Constructeur du service
     *
     * @param mapper    Le mapper JSON utilisé dans l'application
     * @param baseTopic Le topic de base
     */
    public SynchroEventPublisher(ObjectMapper mapper,
                                 @Value(MQTT_BASE_TOPIC_PROP) String baseTopic) {
        super(mapper, baseTopic);
    }

    @Override
    protected void serviceInitialized() {
        LOG.info("Starting synchronization producer service, broker : {}, base topic : {}", brokerUrl, baseTopic);
    }


    @Override
    protected void onMessageReceived(MqttMessage message, String topic) {

    }

    /**
     * Publie un évènement global
     *
     * @param event L'évènement à publier
     */
    public void publishEvent(UpdateEvent event) {
        publishMessage(baseTopic, event);
    }

    /**
     * Publie un évènement spécifique à l'intervention indiquée
     *
     * @param event    L'évènement à publier
     * @param intervId L'id de l'intervention pour laquelle l'évènement est émis
     */
    public void publishEvent(UpdateEvent event, Long intervId) {
        if (intervId == null || intervId <= 0)
            throw new IllegalArgumentException("L'id d'intervention doit être non nul");

        String topic = baseTopic + "/" + intervId;
        publishMessage(topic, event);
    }
}
