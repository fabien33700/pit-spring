package fr.istic.pit.backend.services.mqtt.drone.model;

import fr.istic.pit.backend.services.mqtt.AbstractEvent;

/**
 * Évènement en provenance du drone
 */
public final class DroneEvent extends AbstractEvent<DroneEventType> {

}
