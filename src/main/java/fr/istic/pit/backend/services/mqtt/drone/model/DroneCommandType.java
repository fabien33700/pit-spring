package fr.istic.pit.backend.services.mqtt.drone.model;

/**
 * Le type de commande
 */
public enum DroneCommandType {
    /**
     * Aller au prochain point
     */
    NEXT_POINT,

    /**
     * Déconnexion du drone
     */
    LOGOUT,

    /**
     * Retour à la zone d'atterissage
     */
    RTL,

    /**
     * Décollage
     */
    ARM
}
