package fr.istic.pit.backend.services.mappers;

import fr.istic.pit.backend.utils.Utils;

import java.util.*;

/**
 * Décrit le comportement de base d'un mapper.
 * Un « mapper » permet de convertir un objet en un autre, généralement une entité JPA
 * vers l'objet DTO associé.
 *
 * @param <S> Le type de l'objet source
 * @param <T> Le type de l'objet target
 */
public interface Mapper<S, T> {
    /**
     * Convertit un objet source vers le type cible.
     *
     * @param source L'objet source
     * @return Un nouvel objet au type cible
     */
    T map(S source);

    /**
     * Convertit un objet de type cible vers le type source.
     *
     * @param target L'objet cible
     * @return Un nouvel objet au type source
     */
    S reverseMap(T target);

    /**
     * Convertit une liste d'objets sources en une liste
     * d'objets au type cible.
     *
     * @param sourceList La liste source
     * @return Une liste contenant les objets au type cible
     */
    default List<T> mapList(Iterable<S> sourceList) {
        return Utils.mapList(sourceList, ArrayList::new, this::map);
    }

    /**
     * Convertit une liste d'objets sources en une liste
     * d'objets au type cible.
     *
     * @param sourceList La liste source
     * @return Un ensemble contenant les objets au type cible
     */
    default Set<T> mapSet(Iterable<S> sourceList) {
        return Utils.mapList(sourceList, HashSet::new, this::map);
    }

    /**
     * Convertit une liste d'objets cibles en une liste
     * d'objets au type source.
     *
     * @param targetList La liste cible
     * @return Une liste contenant les objets au type source
     */
    default List<S> reverseMapList(Iterable<T> targetList) {
        return Utils.mapList(targetList, ArrayList::new, this::reverseMap);
    }

    /**
     * Convertit une liste d'objets cibles en une liste
     * d'objets au type source.
     *
     * @param targetList La liste cible
     * @return Un ensemble contenant les objets au type source
     */
    default Set<S> reverseMapSet(Iterable<T> targetList) {
        return Utils.mapList(targetList, HashSet::new, this::reverseMap);
    }

    /**
     * Tente de créer une énumération à partir d'une constante au format String.
     *
     * @param enumType La classe de l'énumération (E.class)
     * @param constant La constante en chaîne de caractères
     * @param <E>      Le type de l'énumération
     * @return La valeur de l'énumération trouvée
     * @throws IllegalArgumentException Cette constante n'existe pas pour ce type d'énumération
     */
    default <E extends Enum<E>> E parseEnum(Class<E> enumType, String constant) {
        try {
            return Enum.valueOf(enumType, constant);
        } catch (IllegalArgumentException ex) {
            throw new IllegalArgumentException("Unable to parse value \"" + constant + "\", allowed values are " +
                EnumSet.allOf(enumType).toString());
        }
    }

    /**
     * Affiche une énumération au format chaîne de caractères.
     *
     * @param enumValue La constante d'énumération
     * @return La constante de l'énumération, ou null
     */
    default String printEnum(Enum<?> enumValue) {
        if (enumValue == null)
            return null;

        return enumValue.name();
    }
}
