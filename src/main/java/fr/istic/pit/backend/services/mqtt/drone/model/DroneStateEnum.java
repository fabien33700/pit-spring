package fr.istic.pit.backend.services.mqtt.drone.model;

/**
 * L'état du drone
 */
public enum DroneStateEnum {
    /**
     * LE drone est au sol
     */
    GROUND,

    /**
     * Le drone se déplace vers un point
     */
    MOVING,

    /**
     * Le drone vole de manière stationnaire
     */
    IDLE
}
