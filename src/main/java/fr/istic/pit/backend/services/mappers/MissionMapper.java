package fr.istic.pit.backend.services.mappers;

import fr.istic.pit.backend.model.dto.MissionDTO;
import fr.istic.pit.backend.model.entities.Mission;
import org.springframework.stereotype.Service;

/**
 * Mapper pour les missions
 */
@Service
public class MissionMapper implements Mapper<Mission, MissionDTO> {

    /**
     * Le mapper pour les points de mission
     */
    private final DronePointMapper dronePointMapper;

    public MissionMapper(DronePointMapper dronePointMapper) {
        this.dronePointMapper = dronePointMapper;
    }

    @Override
    public MissionDTO map(Mission source) {
        MissionDTO dto = new MissionDTO();
        dto.id = source.getId();
        dto.currentPointId = source.getCurrentPointId();
        dto.intervId = source.getIntervention().getId();
        dto.points = dronePointMapper.mapList(source.getPoints());
        dto.missionLoop = source.isMissionLoop();
        dto.terminated = source.isTerminated();
        return dto;
    }

    @Override
    public Mission reverseMap(MissionDTO dto) {
        Mission entity = new Mission.Builder()
            .currentPointId(dto.currentPointId)
            .build();
        dronePointMapper.reverseMapList(dto.points)
            .forEach(entity::addDronePoint);
        return entity;
    }
}
