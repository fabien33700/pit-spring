package fr.istic.pit.backend.services.mqtt.drone.model;

import static java.lang.String.format;

/**
 * Situation du drone
 */
public class DroneState {
    /**
     * état du drone (Ground, Moving, Idle)
     */
    public final DroneStateEnum state;

    /**
     * Latitude du drone
     */
    public final double latitude;

    /**
     * Longitude du drone
     */
    public final double longitude;

    /**
     * Altitude du drone
     */
    public final double altitude;

    /**
     * Constructeur complet
     *
     * @param state     État du drone
     * @param latitude  Latitude
     * @param longitude Longitude
     * @param altitude  Altitude
     */
    public DroneState(DroneStateEnum state, double latitude, double longitude, double altitude) {
        this.state = state;
        this.latitude = latitude;
        this.longitude = longitude;
        this.altitude = altitude;
    }

    @Override
    public String toString() {
        return format("State : %s, Latitude : %f, Longitude : %f, Altitude : %f",
            state.name(), latitude, longitude, altitude);
    }
}
