package fr.istic.pit.backend.services.mqtt.sync;

import fr.istic.pit.backend.services.mqtt.sync.model.UpdateEventType;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 * Si une méthode annotée du contrôleur est appelée,
 * elle émettra un évènement du type précisé par la méthode value()
 */
@Retention(RUNTIME)
@Target(METHOD)
public @interface EmitUpdateEvent {
    /**
     * Le type d'évènement de mise à jour à notifier
     *
     * @return Le type d'évènement
     */
    UpdateEventType value();
}
