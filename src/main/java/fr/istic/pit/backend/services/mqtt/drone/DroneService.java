package fr.istic.pit.backend.services.mqtt.drone;

import com.fasterxml.jackson.databind.ObjectMapper;
import fr.istic.pit.backend.model.entities.DronePoint;
import fr.istic.pit.backend.model.entities.Intervention;
import fr.istic.pit.backend.model.entities.Mission;
import fr.istic.pit.backend.repositories.InterventionRepository;
import fr.istic.pit.backend.repositories.MissionRepository;
import fr.istic.pit.backend.services.mqtt.AbstractMqttService;
import fr.istic.pit.backend.services.mqtt.drone.model.*;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.time.LocalDateTime;
import java.util.*;
import java.util.concurrent.atomic.AtomicLong;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Stream;

import static fr.istic.pit.backend.model.enums.DroneAvailability.*;
import static fr.istic.pit.backend.services.mqtt.drone.DroneService.AfterStopAction.SEND_LOGOUT;
import static fr.istic.pit.backend.services.mqtt.drone.DroneService.AfterStopAction.SEND_RTL;
import static fr.istic.pit.backend.services.mqtt.drone.model.DroneCommandType.*;
import static fr.istic.pit.backend.services.mqtt.drone.model.DroneEventType.*;
import static fr.istic.pit.backend.services.mqtt.drone.model.DroneStateEnum.GROUND;
import static fr.istic.pit.backend.utils.Utils.isConstantOf;
import static java.lang.Double.parseDouble;
import static java.util.Objects.requireNonNull;
import static java.util.regex.Pattern.compile;
import static java.util.regex.Pattern.quote;

/**
 * Service de gestion du drone et de ses missions
 */
@Service
public class DroneService extends AbstractMqttService {

    /**
     * Logger
     */
    private static final Logger missionLogger = LoggerFactory.getLogger("Drone Mission Logger");

    /**
     * Topic de base par défaut
     */
    protected static final String DEFAULT_MQTT_BASE_TOPIC = "pit/sync/events";

    /**
     * Placeholder Spring de la propriété pour le topic de base
     */
    private static final String MQTT_BASE_TOPIC_PROP = "${pit-backend.mqtt.topic:" + DEFAULT_MQTT_BASE_TOPIC + "}";

    /**
     * Suffixe du topic pour les commandes envoyées au drone
     */
    private static final String MQTT_CMD_SUFFIX = "/commands";

    /**
     * Compteur pour les numéros d'évènements
     */
    private final AtomicLong counter = new AtomicLong(0);

    /**
     * Map contenant les contrôleurs de mission par intervention
     */
    private final Map<Long, MissionControl> runningMissions;

    /**
     * Le dernier état connu du drone
     */
    private DroneState lastDroneState;

    /**
     * Liste des interventions pour lesquelles un RTL est en cours
     */
    private final Set<Long> pendingRTL;

    /*
     * Dépendances aux autres services
     */
    private final InterventionRepository intervRepository;
    private final MissionRepository missionRepository;

    /**
     * Constructeur du service
     *
     * @param mapper            Le mapper JSON de l'application
     * @param baseTopic         Le topic de base
     * @param intervRepository  Le repository d'intervention
     * @param missionRepository Le repository de mission
     */
    public DroneService(ObjectMapper mapper,
                        @Value(MQTT_BASE_TOPIC_PROP) String baseTopic,
                        InterventionRepository intervRepository,
                        MissionRepository missionRepository) {
        super(mapper, baseTopic);
        this.intervRepository = intervRepository;
        this.missionRepository = missionRepository;
        this.pendingRTL = new HashSet<>();
        runningMissions = new HashMap<>();
    }

    /**
     * Construit le topic propre à la mission à partir de l'id d'intervention
     *
     * @param intervId L'id de l'intervention
     * @return Le topic spécifique au drone
     */
    private String buildTopic(long intervId) {
        return baseTopic + "/" + intervId + "/drone";
    }

    /**
     * Récupère l'id de l'intervention à partir du topic du message reçu ?
     *
     * @param topic Le topic du message
     * @return L'id de l'intervention du topic, ou null
     */
    private Long parseTopic(String topic) {
        /* Expression régulière pour récupérer l'id de l'intervention à partir du topic
         * d'évènement du drone : {baseTopic}/[id]/drone */
        Pattern regex = compile(quote(baseTopic + "/") + "(\\d+)" + quote("/drone"));
        Matcher matcher = regex.matcher(topic);

        if (!matcher.find() || matcher.group(1) == null)
            return null;

        return Long.parseLong(matcher.group(1));
    }

    /**
     * Enregistre un drone auprès d'une intervention
     *
     * @param intervId L'id de l'intervention
     */
    public void registerDrone(Long intervId) {
        Intervention interv = intervRepository.getOne(intervId);

        if (interv.getDroneAvailability() == UNAVAILABLE) {
            throw new IllegalStateException("The drone for intervention " + interv + " is unavailable.");
        }

        interv.setDroneAvailability(AVAILABLE);
        intervRepository.save(interv);
    }

    /**
     * Créer une mission
     *
     * @param intervId      L'id de l'intervention
     * @param missionLoop   Mode de déroulé (true = boucle, false = A/R)
     * @param missionPoints Les points de la mission
     * @return La mission nouvellement créée
     */
    public Mission createMission(Long intervId, boolean missionLoop, List<DronePoint> missionPoints) {
        Intervention interv = intervRepository.findById(intervId)
            .orElseThrow(EntityNotFoundException::new);

        if (pendingRTL.contains(intervId)) {
            missionLogger.warn("Intervention {} : cannot create a new mission because the drone" +
                " is returning to launch and has not landed yet.", intervId);
            throw new IllegalStateException("Please wait until the drone has returned to launch.");
        }

        /* Si une mission était en cours dans la base de données, on l'archive pour ne pas
         * bloquer la planification d'une nouvelle mission dans cette session */
        Optional.ofNullable(interv.getCurrentMission())
            .map(Mission::getId)
            .flatMap(missionRepository::findById)
            .ifPresent(last -> {
                last.setTerminated(true);
                missionRepository.save(last);
                missionLogger.warn("A non-terminated mission from a previous session" +
                        " was found for intervention {}. It had been automatically terminated. (id: {})",
                    intervId, last.getId());
            });

        int order = 0;
        for (DronePoint point : missionPoints)
            point.setOrderNumber(order++);

        Mission mission = new Mission.Builder()
            .intervention(interv)
            .missionLoop(missionLoop)
            .build();
        interv.setCurrentMission(mission);

        missionPoints.forEach(mission::addDronePoint);
        mission = missionRepository.save(mission);
        intervRepository.save(interv);

        // Lancement de la mission
        MissionControl missionControl = new MissionControl(mission, missionLoop);
        runningMissions.put(mission.getIntervention().getId(), missionControl);
        return mission;
    }

    @Override
    protected void serviceInitialized() {
        LOG.info("Starting drone pub-sub communication service, broker : {}, base topic : {}", brokerUrl, baseTopic);
    }

    /**
     * Réception de message MQTT
     *
     * @param message Le message reçu
     * @param topic   Le topic de provenance
     */
    @Override
    protected void onMessageReceived(MqttMessage message, String topic) {
        Long intervId = parseTopic(topic);
        if (intervId == null || intervId == 0) {
            LOG.warn("Could not extract intervention id from the topic : {}", topic);
        } else {
            byte[] payload = message.getPayload();
            try {
                DroneEvent droneEvent = mapper.readValue(payload, DroneEvent.class);
                if (droneEvent.eventType == RTL_DONE) {
                    missionLogger.info("Drone successfully returned to launch for intervention {}", intervId);
                    pendingRTL.remove(intervId);
                } else if (!runningMissions.containsKey(intervId)) {
                    missionLogger.debug("Received a drone event for intervention {}, " +
                        "but there is no mission in progress for it in the system", intervId);
                } else {
                    MissionControl missionControl = runningMissions.get(intervId);
                    if (droneEvent.eventType.isMissionRelated())
                        missionControl.notifyNextMissionAction(droneEvent);

                    if (droneEvent.eventType.isExternal())
                        missionControl.notifyExternalEvent(droneEvent);
                }
            } catch (IOException ex) {
                LOG.warn("Following message could not be parsed as a drone event : \n{}, because : {}",
                    new String(payload, StandardCharsets.UTF_8) + ", because : ", ex.toString());
            }
        }
    }

    public Optional<Mission> getCurrentMission(Long intervId) {
        return intervRepository.findById(intervId)
            .map(Intervention::getCurrentMission);
    }

    public DroneState getLastDroneState() {
        return lastDroneState;
    }

    /**
     * Interrompt la mission.
     *
     * @param intervId L'id de l'intevention
     * @param action   L'action à effectuer après l'avoir stoppé
     */
    public void stopMission(Long intervId, AfterStopAction action) {
        Intervention interv = intervRepository.findById(intervId)
            .orElseThrow(EntityNotFoundException::new);

        /*
         * Y a-t-il une mission en cours ?
         */
        boolean runningMission =
            interv.getCurrentMission() != null &&
                !interv.getCurrentMission().isTerminated() &&
                runningMissions.containsKey(intervId);
        if (runningMission) {
            runningMissions.get(intervId).stop();
            runningMissions.remove(intervId);
        }

        String commandTopic = buildTopic(intervId) + MQTT_CMD_SUFFIX;
        if (action == SEND_RTL) {
            publishMessage(commandTopic, createCommand(RTL));
            missionLogger.info("Sent RTL command to the drone on intervention {}.", intervId);
            pendingRTL.add(intervId);
        } else if (action == SEND_LOGOUT && interv.getDroneAvailability() != NONE) {
            if (interv.getDroneAvailability() == AVAILABLE) {
                interv.setDroneAvailability(UNAVAILABLE);
                intervRepository.save(interv);
            }
            publishMessage(commandTopic, createCommand(LOGOUT));
            missionLogger.info("Sent LOGOUT command to the drone on intervention {}.", intervId);
        }
    }

    /**
     * Désigne une action qui se déclenche lorsque
     * la condition de l'action précédente a été satisfaite.
     */
    public interface ReturnAction {
        /**
         * Exécute l'action
         */
        void execute();

        /**
         * Indique si l'évènement entrant complète l'action en cours
         *
         * @param event Un évènement reçu du drone
         * @return retourne true si l'action est complétée, false sinon
         */
        boolean isActionComplete(DroneEvent event);

        /**
         * Méthode exécutée après que l'action aie été complétée
         *
         * @param event L'évènement qui a terminé l'action
         */
        void afterComplete(DroneEvent event);

        /**
         * Indique si l'action est répétable.
         * S'il ne l'est pas, elle sera supprimée de la boucle des actions après exécution
         *
         * @return true, l'action est répétable, false sinon
         */
        default boolean repeatable() {
            return false;
        }
    }

    /**
     * Créer une commande du type spécifié
     *
     * @param type Le type de la commande
     * @return La commande
     */
    private DroneCommand createCommand(DroneCommandType type) {
        return createCommand(type, new HashMap<>());
    }

    /**
     * Créer une commande du type spécifié avec des données supplémentaires
     *
     * @param type Le type de la commande
     * @param data Les données supplémentaires
     * @return La commande
     */
    private DroneCommand createCommand(DroneCommandType type, Map<String, Object> data) {
        DroneCommand command = new DroneCommand();
        command.eventType = type;
        command.data.putAll(data);
        command.eventNo = counter.incrementAndGet();
        command.eventTime = LocalDateTime.now();
        return command;
    }

    /**
     * Contrôleur de mission
     */
    public class MissionControl {
        /**
         * Les données de la mission enregistrées en base de données
         */
        private final Mission mission;

        /**
         * Le topic pour les commandes envoyées au drone
         */
        private final String commandTopic;

        /**
         * La liste des actions constituant la mission du drone
         */
        private final List<DroneService.ReturnAction> actions = new ArrayList<>();

        /**
         * Mode de déroulé (true = boucle, false = A/R)
         */
        private final boolean loopMode;

        /**
         * Sens de parcours
         * (1 : aller, -1 : retour)
         */
        private int orientation = 1;

        /**
         * Indice de l'action courante
         */
        private int currentActionIndex;

        /**
         * Créer un contrôleur de mission
         *
         * @param mission     La mission
         * @param missionLoop Mode de déroulé (true = boucle, false = A/R)
         */
        MissionControl(Mission mission, boolean missionLoop) {
            this.mission = requireNonNull(mission, "mission is null");
            this.loopMode = missionLoop;

            // Inscription au topic
            String droneTopic = buildTopic(mission.getIntervention().getId());
            subscribe(droneTopic);
            this.commandTopic = droneTopic + MQTT_CMD_SUFFIX;

            missionLog("Mission scheduled, waiting for message DRONE_STATE from drone to start.");
            actions.add(new WaitForDroneAction());
        }

        /**
         * Stoppe le contrôle de la mission
         */
        void stop() {
            mission.setTerminated(true);
            mission.setCurrentPointId(null);
            missionRepository.save(mission);

            missionLog("Mission ended.");
        }

        /**
         * Construit le déroulé de la mission
         */
        void buildMission() {
            missionLog("Drone is up ! {}", lastDroneState);

            mission.setCurrentPointId(null);
            missionRepository.save(mission);

            // Planification du décollage si le drone est au sol
            if (lastDroneState.state == GROUND)
                actions.add(new ArmAction());

            // On planifie la mission
            mission.getPoints().stream()
                .map(MoveToPointAction::new)
                .forEach(actions::add);
        }

        /**
         * Notifie le contrôleur d'un nouveau message en provenance du drone,
         * relatif à la mission.
         *
         * @param inputEvent L'évènement provenant du drone
         */
        void notifyNextMissionAction(DroneEvent inputEvent) {
            // Récupération de l'action courante
            DroneService.ReturnAction currentAction = actions.get(currentActionIndex);

            // Si l'action est complétée par le message entrant
            if (currentAction.isActionComplete(inputEvent)) {
                currentAction.afterComplete(inputEvent);
                // Si l'action est répétable
                if (currentAction.repeatable())
                    // Incrémentation de l'action courante
                    gotoNextAction();
                else
                    // On supprime l'action de la liste
                    actions.remove(currentActionIndex);

                // Exécution de la prochaine action
                actions.get(currentActionIndex).execute();
            }
        }

        private void gotoNextAction() {
            boolean atFirstPoint = currentActionIndex == 0;
            boolean atLastPoint = currentActionIndex == actions.size() - 1;

            // Bouclage de la liste des actions
            if (loopMode) {
                if (atLastPoint) {
                    missionLog("End of mission reached, going to the first point.");
                    currentActionIndex = 0;
                } else
                    currentActionIndex++;
            }
            if (!loopMode) {
                if (atLastPoint) {
                    missionLog("End of mission reached, going back to the first point by replaying reversely the route.");
                    orientation = -1;
                }
                if (atFirstPoint) {
                    missionLog("Start of mission reached, going to the last point by playing the route.");
                    orientation = 1;
                }
                currentActionIndex += orientation;
            }
        }

        /**
         * Notifie le contrôleur d'un évènement en provenance du drone, extérieur
         * à la mission.
         *
         * @param event L'évènement provenant du drone
         */
        void notifyExternalEvent(DroneEvent event) {
            // Atterrissage d'urgence
            if (event.eventType == EMERGENCY_RTL) {
                stop();
                runningMissions.remove(mission.getIntervention().getId());
                mission.getIntervention().setDroneAvailability(UNAVAILABLE);
                intervRepository.save(mission.getIntervention());
                missionLog("Drone emitted an emergency RTL signal and will become unavailable for the mission");
            }
            // Récupération périodique de l'état du drone
            else if (event.eventType == DRONE_STATE) {
                DroneState state = parseDroneState(event);
                if (state != null) {
                    // Mise à jour de la dernière position du drone
                    lastDroneState = state;
                    missionLogger.trace("Drone new state : {}", lastDroneState);
                }
            }
        }

        /**
         * Analyse l'état du drone dans un message de type DRONE_STATE.
         *
         * @param event Évènement reçu du drone
         * @return Un bean contenant les informations sur l'état du drone
         */
        private DroneState parseDroneState(DroneEvent event) {
            try {
                DroneStateEnum state = DroneStateEnum.valueOf(event.data.getOrDefault("state", "").toString());
                double latitude = parseDouble(event.data.getOrDefault("latitude", "0.0").toString());
                double longitude = parseDouble(event.data.getOrDefault("longitude", "0.0").toString());
                double altitude = parseDouble(event.data.getOrDefault("altitude", "0.0").toString());

                return new DroneState(state, latitude, longitude, altitude);
            } catch (Exception ex) {
                LOG.debug("Error parsing drone state", ex);
                return null;
            }
        }

        /**
         * Log spécifique à la mission
         *
         * @param message Le message
         * @param args    Les arguments
         */
        private void missionLog(String message, Object... args) {
            message = "Mission {}, Intervention {} : " + message;
            Object[] missionInfo = new Object[]{mission.getId(), mission.getIntervention().getId()};
            args = Stream.concat(Arrays.stream(missionInfo), Arrays.stream(args))
                .toArray(Object[]::new);
            missionLogger.info(message, args);
        }

        /* *** Implémentations des actions *** */

        /**
         * Action de décollage du drone
         */
        class ArmAction implements ReturnAction {
            @Override
            public void execute() {
                // Envoi de la commande ARM
                DroneCommand command = createCommand(ARM);
                DroneService.this.publishMessage(commandTopic, command);
                missionLog("Starting mission, ARM command sent to the drone.");
            }

            @Override
            public boolean isActionComplete(DroneEvent event) {
                // Le drone a renvoyé ARM_DONE, le décollage est effectué
                return event.eventType == ARM_DONE;
            }

            @Override
            public void afterComplete(DroneEvent event) {
                missionLog("Drone armed.");
            }
        }

        /**
         * Action d'attente de l'état du drone avant début de la mission
         */
        class WaitForDroneAction implements ReturnAction {
            @Override
            public void execute() {
            }

            @Override
            public boolean isActionComplete(DroneEvent event) {
                // Le drone a envoyé au moins un message DRONE_STATE
                return event.eventType == DRONE_STATE &&
                    event.data.containsKey("state") &&
                    isConstantOf(DroneStateEnum.class, event.data.get("state").toString(), false);
            }

            @Override
            public void afterComplete(DroneEvent event) {
                DroneState droneState = parseDroneState(event);
                if (droneState == null)
                    return; // Ne dois pas arriver !

                lastDroneState = droneState;
                buildMission();
            }
        }

        /**
         * Action de déplacement vers un point
         */
        class MoveToPointAction implements ReturnAction {
            /**
             * Le point vers lequel se déplacer
             */
            private final DronePoint point;

            public MoveToPointAction(DronePoint point) {
                this.point = point;
            }

            @Override
            public void execute() {
                // Création de la commande et envoi du prochain point au drone
                Map<String, Object> data = new HashMap<>();
                data.put("latitude", point.getLatitude());
                data.put("longitude", point.getLongitude());
                data.put("altitude", point.getAltitude());
                DroneCommand command = createCommand(NEXT_POINT, data);

                DroneService.this.publishMessage(commandTopic, command);
            }

            @Override
            public boolean isActionComplete(DroneEvent event) {
                // L'action est complétée lorsque le drone est arrivé au point
                return event.eventType == ARRIVE_TO_POINT;
            }

            @Override
            public void afterComplete(DroneEvent responseEvent) {
                // Mise à jour de la mission en base de données
                mission.setCurrentPointId(point.getId());
                missionRepository.save(mission);

                missionLog("Drone arrived to point [{}/{}] : latitude: {}, longitude: {} at {}",
                    point.getOrderNumber() + 1, mission.getPoints().size(),
                    point.getLatitude(), point.getLongitude(), responseEvent.eventTime
                );
            }

            @Override
            public boolean repeatable() {
                return true;
            }
        }
    }

    /**
     * Action possible après l'interruption d'une mission
     */
    public enum AfterStopAction {
        /**
         * Ne rien faire
         */
        DO_NOTHING,

        /**
         * Envoyer un RTL (retour à la zone de décollage)
         */
        SEND_RTL,

        /**
         * Envoyer un LOGOUT
         */
        SEND_LOGOUT
    }
}
