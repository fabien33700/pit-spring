package fr.istic.pit.backend.services;

import java.util.List;
import java.util.Optional;

/**
 * Interface de base décrivant le comportement d'un service de gestion de données
 *
 * @param <T>  Le type d'entité
 * @param <ID> Le type de l'identifiant technique de l'entité
 */
public interface DataService<T, ID> {
    /**
     * Récupérer toutes les entités de la base
     *
     * @return Une liste de toutes les entités
     */
    List<T> getAll();

    /**
     * Trouve une entité par son identifiant technique.
     *
     * @param id L'identifiant technique
     * @return Un Optional contenant potentiellement l'entité trouvée
     */
    Optional<T> findById(ID id);

    /**
     * Insère une entité en base de données.
     *
     * @param entity L'entité à insérer
     * @return L'instance de l'entité sauvegardée en base de données
     */
    T create(T entity);

    /**
     * Supprime une entité de la base de données
     *
     * @param id L'identifiant technique de l'entité à supprimer
     */
    void delete(Long id);

    /**
     * Met à jour une entité en base de données.
     *
     * @param id      L'identifiant technique de l'entité
     * @param updates Les modifications à apporter à l'entité
     * @return L'entité mise à jour
     */
    T update(ID id, T updates);
}
