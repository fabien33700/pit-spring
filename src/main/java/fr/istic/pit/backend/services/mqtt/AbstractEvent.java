package fr.istic.pit.backend.services.mqtt;

import com.fasterxml.jackson.annotation.JsonInclude;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;

import static com.fasterxml.jackson.annotation.JsonInclude.Include.NON_EMPTY;

/**
 * Classe abstraite pour les évènements MQTT
 *
 * @param <T> L'énumération décrivant le type de l'évènement
 */
@JsonInclude(NON_EMPTY)
public abstract class AbstractEvent<T extends Enum<T>> {
    /**
     * Le type d'évènement
     */
    public T eventType;

    /**
     * La date d'émission
     */
    public LocalDateTime eventTime;

    /**
     * Le numéro d'évènement
     */
    public Long eventNo;

    /**
     * Les données supplémentaires
     */
    public final Map<String, Object> data = new HashMap<>();
}
