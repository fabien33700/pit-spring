package fr.istic.pit.backend.services;

import fr.istic.pit.backend.model.dto.PointOfInterestDTO;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import javax.annotation.PostConstruct;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * Service de communication avec le SIG
 */
@Service
public class SIGService {

    /**
     * URL de base du SIG à interroger
     * (défini dans application.properties)
     */
    @Value("${pit-project.sig.endpoint:http://localhost:8080/sig}")
    private String sigUrl;

    /**
     * Client HTTP
     */
    private RestTemplate restTemplate;

    @PostConstruct
    public void init() {
        restTemplate = new RestTemplate();
    }

    /**
     * Récupérer la liste des points d'intérêts fixes pour des coordonnées
     * géographiques et un rayon de recherche donnés
     *
     * @param latitude  La latitude du point
     * @param longitude La longitude du point
     * @param radius    Le rayon dans lequel rechercher des POI
     * @return La liste des points d'intérêts
     */
    public List<PointOfInterestDTO> getPointsOfInterests(double latitude, double longitude, double radius) {
        UriComponentsBuilder uriBuilder = UriComponentsBuilder.fromHttpUrl(sigUrl)
            .path("/")
            .queryParam("latitude", latitude)
            .queryParam("longitude", longitude)
            .queryParam("radius", radius);

        PointOfInterestDTO[] pois = restTemplate
            .getForObject(uriBuilder.toUriString(), PointOfInterestDTO[].class);

        if (pois == null) {
            return Collections.emptyList();
        }
        return Arrays.asList(pois);
    }
}
