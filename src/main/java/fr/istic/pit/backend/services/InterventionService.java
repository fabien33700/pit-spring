package fr.istic.pit.backend.services;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import fr.istic.pit.backend.model.dto.PointOfInterestDTO;
import fr.istic.pit.backend.model.entities.AbstractMapPoint;
import fr.istic.pit.backend.model.entities.Intervention;
import fr.istic.pit.backend.model.entities.PointOfInterest;
import fr.istic.pit.backend.model.entities.Resource;
import fr.istic.pit.backend.model.state.MacroState;
import fr.istic.pit.backend.model.state.Transition;
import fr.istic.pit.backend.repositories.InterventionRepository;
import fr.istic.pit.backend.repositories.PointOfInterestRepository;
import fr.istic.pit.backend.repositories.ResourceRepository;
import fr.istic.pit.backend.services.mappers.PointOfInterestMapper;
import fr.istic.pit.backend.services.mqtt.drone.DroneService;
import fr.istic.pit.backend.utils.Utils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import javax.persistence.EntityNotFoundException;
import javax.transaction.Transactional;
import java.time.LocalDateTime;
import java.util.*;

import static fr.istic.pit.backend.model.entities.Intervention.ALP;
import static fr.istic.pit.backend.model.entities.Resource.*;
import static fr.istic.pit.backend.model.state.MacroState.*;
import static fr.istic.pit.backend.model.state.MicroState.IDLE;
import static fr.istic.pit.backend.model.state.MicroState.MOVING;
import static fr.istic.pit.backend.services.mqtt.drone.DroneService.AfterStopAction.SEND_LOGOUT;
import static java.lang.String.format;
import static java.util.Arrays.asList;
import static java.util.Objects.requireNonNull;

/**
 * Service de gestion des interventions
 */
@Service
public class InterventionService implements DataService<Intervention, Long> {
    private static final Logger LOG = LoggerFactory.getLogger(InterventionService.class);

    /**
     * Chemin du fichier contenant les rôles par défaut par type de moyen
     */
    public static final String DEFAULT_ROLES_PATH = "classpath:values/default_roles.json";

    /**
     * Repository des interventions
     */
    private final InterventionRepository intervRepository;

    /**
     * Mapper de point d'intérêt
     */
    private final PointOfInterestMapper pointOfInterestMapper;

    /**
     * Repository des points d'intérêt
     */
    private final PointOfInterestRepository pointOfInterestRepository;

    /**
     * Repository des moyens
     */
    private final ResourceRepository resourceRepository;

    /**
     * Service d'accès au SIG
     */
    private final SIGService sigService;

    /**
     * Service de gestion du drone
     */
    private final DroneService droneService;

    /**
     * Rôles par défaut par type de moyens
     */
    private Map<String, Integer> defaultRoles = new HashMap<>();

    /**
     * Constructeur du service.
     *
     * @param intervRepository          Repository des interventions
     * @param pointOfInterestMapper     Mapper de point d'intérêt
     * @param pointOfInterestRepository Repository des points d'intérêt
     * @param resourceRepository        Repository des moyens
     * @param sigService                Service d'accès au SIG
     * @param droneService              Service de gestion du drone
     */
    public InterventionService(InterventionRepository intervRepository,
                               PointOfInterestMapper pointOfInterestMapper,
                               PointOfInterestRepository pointOfInterestRepository,
                               ResourceRepository resourceRepository,
                               SIGService sigService,
                               DroneService droneService) {
        this.intervRepository = intervRepository;
        this.pointOfInterestMapper = pointOfInterestMapper;
        this.pointOfInterestRepository = pointOfInterestRepository;
        this.resourceRepository = resourceRepository;
        this.sigService = sigService;
        this.droneService = droneService;
    }

    @PostConstruct
    public void init() {
        loadDefaultRoles();
    }

    /**
     * Fichier contenant les rôles par défaut
     */
    @Value(DEFAULT_ROLES_PATH)
    org.springframework.core.io.Resource defaultRolesRes;

    private void loadDefaultRoles() {
        // Chargement des rôles par défaut depuis le fichier
        try {
            String content = Utils.readResource(defaultRolesRes);
            ObjectMapper mapper = new ObjectMapper();
            defaultRoles = mapper.readValue(content, new TypeReference<Map<String, Integer>>() {
            });
        } catch (Exception e) {
            LOG.warn("Could not load default values because :", e);
        }
    }

    @Override
    public List<Intervention> getAll() {
        return intervRepository.findAll();
    }

    @Override
    public Optional<Intervention> findById(Long id) {
        return intervRepository.findById(id);
    }

    @Override
    public Intervention create(Intervention interv) {
        interv.setStartTime(LocalDateTime.now());
        importPointsFromSig(interv);

        // Attribution des moyens initiaux de l'intervention par le Codis
        interv.getResources().forEach(res ->
            initializeResource(interv, res, MacroState.Start::create));

        return intervRepository.save(interv);
    }

    /**
     * Récupération des POI depuis le SIG.
     *
     * @param interv L'intervention
     */
    private void importPointsFromSig(Intervention interv) {
        List<PointOfInterestDTO> points = sigService.getPointsOfInterests(
            interv.getLatitude(), interv.getLongitude(), 0);

        for (PointOfInterestDTO point : points)
            interv.addPointOfInterest(pointOfInterestMapper.reverseMap(point));
    }

    /**
     * Clôture l'intervention.
     *
     * @param id L'id de l'intervention
     */
    @Override
    public void delete(Long id) {
        Intervention interv = intervRepository.getOne(id);
        interv.setActive(false);
        interv.setEndTime(LocalDateTime.now());
        droneService.stopMission(id, SEND_LOGOUT);

        intervRepository.save(interv);
    }

    /**
     * Mise à jour d'une intervention
     *
     * @param id      L'id de l'intervention
     * @param updates Les modifications à apporter
     */
    @Override
    public Intervention update(Long id, Intervention updates) {
        Intervention actual = intervRepository.getOne(id);
        actual.merge(updates);

        return intervRepository.save(actual);
    }

    /**
     * Ajoute un POI à une intervention
     *
     * @param intervId        L'id de l'intervention
     * @param pointOfInterest Le POI à créer
     * @return Le POI créé
     */
    public PointOfInterest addPointOfInterest(Long intervId, PointOfInterest pointOfInterest) {
        requireNonNull(pointOfInterest, "Point of interest is null");
        Intervention interv = intervRepository.getOne(intervId);
        interv.addPointOfInterest(pointOfInterest);

        return pointOfInterestRepository.save(pointOfInterest);
    }

    /**
     * Attribue le rôle par défaut au moyen en fonction de son type
     * si aucun role n'a été spécifié
     *
     * @param interv   L'intervention
     * @param resource Le moyen
     */
    private void assignDefaultRole(Intervention interv, Resource resource) {
        if (resource.getRole() == null || resource.getRole() == 0) {
            // Cas particulier du VLCG
            if (VLCG.equals(resource.getPictoType())) {
                if (ALP.equals(interv.getSinisterCode()))
                    resource.setRole(PERSONNES);
                else
                    resource.setRole(INCENDIE);
            } else {
                String typeRes = resource.getPictoType().toUpperCase();
                int role = defaultRoles.getOrDefault(typeRes, 0);
                resource.setRole(role);
            }
        }
    }

    /**
     * Récupère un point d'intérêt dans une intervention
     *
     * @param intervId L'id de l'intervention
     * @param poiId    L'id du POI
     * @return Le point d'intérêt
     * @throws EntityNotFoundException L'intervention ou le POI n'ont pas été trouvés, ou le
     *                                 POI trouvé n'appartient pas à l'intervention indiquée.
     */
    public PointOfInterest findPOIInIntervention(Long intervId, Long poiId) {
        Intervention interv = intervRepository.getOne(intervId);
        PointOfInterest poi = pointOfInterestRepository.getOne(poiId);
        checkInterventionHasPoint(interv, poi);

        return poi;
    }

    /**
     * Récupère un moyen dans une intervention
     *
     * @param intervId L'id de l'intervention
     * @param resId    L'id du moyen
     * @return Le point d'intérêt
     * @throws EntityNotFoundException L'intervention ou le POI n'ont pas été trouvés, ou le
     *                                 POI trouvé n'appartient pas à l'intervention indiquée.
     */
    public Resource findResourceInIntervention(Long intervId, Long resId) {
        Intervention interv = intervRepository.getOne(intervId);
        Resource resource = resourceRepository.getOne(resId);
        checkInterventionHasPoint(interv, resource);

        return resource;
    }

    /**
     * Demande de moyen en masse par le COS pour une intervention
     *
     * @param intervId  L'id de l'intervention
     * @param resources Les moyens à créer
     */
    @Transactional
    public void bulkAddResources(Long intervId, List<Resource> resources) {
        Intervention interv = intervRepository.getOne(intervId);
        for (Resource resource : resources) {
            initializeResource(interv, resource, MacroState.Start::request);
        }
        resourceRepository.saveAll(resources);
    }

    /**
     * Création d'une demande de moyen par le COS pour une intervention
     *
     * @param intervId L'id de l'intervention
     * @param resource Le moyen à créer
     * @return Le moyen créé
     */
    public Resource addResource(Long intervId, Resource resource) {
        requireNonNull(resource, "Resource is null");
        Intervention interv = intervRepository.getOne(intervId);
        initializeResource(interv, resource, MacroState.Start::request);

        return resourceRepository.save(resource);
    }

    private void initializeResource(Intervention interv, Resource resource,
                                    Transition<MacroState.Start> transition) {
        assignDefaultRole(interv, resource);

        // Initialise la machine à état macro pour le nouveau moyen
        MacroState.State initialState = transition.apply(new MacroState.Start(resource));
        resource.setMacroState(initialState.id());
        interv.addResource(resource);
    }

    /**
     * Déplace un point d'intérêt sur la carte.
     *
     * @param intervId  L'id de l'intervention
     * @param poiId     L'id du point d'intérêt
     * @param latitude  La nouvelle latitude
     * @param longitude La nouvelle longitude
     */
    public void movePointOfInterest(Long intervId, Long poiId, Double latitude, Double longitude) {
        Intervention interv = intervRepository.getOne(intervId);
        PointOfInterest pointOfInterest = pointOfInterestRepository.getOne(poiId);
        checkInterventionHasPoint(interv, pointOfInterest);

        pointOfInterest.setLatitude(latitude);
        pointOfInterest.setLongitude(longitude);
        pointOfInterestRepository.save(pointOfInterest);
    }

    /**
     * Supprime un point d'intérêt.
     *
     * @param intervId L'id de l'intervention
     * @param poiId    L'id du point d'intérêt
     */
    public void removePointOfInterest(Long intervId, Long poiId) {
        Intervention interv = intervRepository.getOne(intervId);
        PointOfInterest poi = pointOfInterestRepository.getOne(poiId);
        checkInterventionHasPoint(interv, poi);
        poi.setActive(false);

        pointOfInterestRepository.save(poi);
    }

    /**
     * Méthode interne permettant de passer un moyen de l'état Waiting
     * aux états Coming (accepté), Cancelled ou Rejected.
     *
     * @param intervId   L'id de l'intervention
     * @param resId      L'id du moyen
     * @param transition La transition permettant de passer de l'état Waiting à l'état désiré
     * @return Le moyen correctement mis à jour
     */
    private Resource modifyWaitingResource(Long intervId, Long resId, Transition<Waiting> transition) {
        Intervention interv = intervRepository.getOne(intervId);
        Resource resource = resourceRepository.getOne(resId);
        checkInterventionHasPoint(interv, resource);

        if (!WAITING.equals(resource.getMacroState()))
            throw new IllegalStateException(format(
                "The macro state of the resource %d is %s, expecting Waiting, could not continue.",
                resource.getId(), resource.getMacroState()));

        MacroState.Waiting state = new MacroState.Waiting(resource);
        MacroState.State newState = transition.apply(state);
        resource.setMacroState(newState.id());

        return resource;
    }

    /**
     * Accepte une demande de moyen (CODIS) et lui attribue un nom
     *
     * @param intervId     L'id de l'intervention
     * @param resId        L'id du moyen
     * @param resourceName Le nom attribué par le Codis au nouveau moyen
     */
    public void acceptResource(Long intervId, Long resId, String resourceName) {
        Resource accepted = modifyWaitingResource(intervId, resId, Waiting::accept);
        accepted.setName(resourceName);

        resourceRepository.save(accepted);
    }

    /**
     * Refuse une demande de moyen (CODIS)
     *
     * @param intervId L'id de l'intervention
     * @param resId    L'id du moyen
     */
    public void rejectResource(Long intervId, Long resId) {
        Resource rejected = modifyWaitingResource(intervId, resId, Waiting::reject);
        resourceRepository.save(rejected);
    }

    /**
     * Annule une demande de moyen (COS)
     *
     * @param intervId L'id de l'intervention
     * @param resId    L'id du moyen
     */
    public void cancelResource(Long intervId, Long resId) {
        Resource cancelled = modifyWaitingResource(intervId, resId, Waiting::cancel);
        resourceRepository.save(cancelled);
    }

    /**
     * Libération d'un moyen (COS)
     *
     * @param intervId L'id de l'intervention
     * @param resId    L'id du moyen
     */
    public void releaseResource(Long intervId, Long resId) {
        Intervention interv = intervRepository.getOne(intervId);
        Resource resource = resourceRepository.getOne(resId);
        checkInterventionHasPoint(interv, resource);

        if (COMING.equals(resource.getMacroState())) {
            MacroState.Coming state = new MacroState.Coming(resource);
            resource.setMacroState(state.release().id());
        } else if (ARRIVED.equals(resource.getMacroState())) {
            MacroState.Arrived state = new MacroState.Arrived(resource);
            resource.setMacroState(state.release().id());
        } else
            throw new IllegalStateException(format(
                "The macro state of the resource %d is %s, expecting COMING or ARRIVED, could not validate.",
                resource.getId(), resource.getMacroState()));

        resourceRepository.save(resource);
    }

    /**
     * Déplace le moyen sur site, met à jour les coordonnées et l'état micro
     *
     * @param intervId  L'id de l'intervention
     * @param resId     L'id du moyen
     * @param latitude  Latitude de la nouvelle position
     * @param longitude Longitude de la nouvelle position
     */
    public void moveResource(Long intervId, Long resId, double latitude, double longitude) {
        Intervention interv = intervRepository.getOne(intervId);
        Resource resource = resourceRepository.getOne(resId);
        checkInterventionHasPoint(interv, resource);

        if (asList(MacroState.FINAL_STATES).contains(resource.getMacroState()))
            throw new IllegalStateException("A resource released, cancelled or rejected cannot be moved.");

        resource.setLatitude(latitude);
        resource.setLongitude(longitude);
        resource.setMicroState(MOVING);
        resourceRepository.save(resource);
    }

    /**
     * Marque le moyen comme arrivé sur place (état macro et micro)
     * Si le moyen est déjà sur site, la mise à jour de l'état macro est ignorée.
     *
     * @param intervId L'id de l'intervention
     * @param resId    L'id du moyen
     */
    public void setResourceArrived(Long intervId, Long resId) {
        Intervention interv = intervRepository.getOne(intervId);
        Resource resource = resourceRepository.getOne(resId);
        checkInterventionHasPoint(interv, resource);

        if (COMING.equals(resource.getMacroState())) {
            MacroState.Coming state = new MacroState.Coming(resource);
            resource.setMacroState(state.arrive().id());
        }
        resource.setMicroState(IDLE);
        resourceRepository.save(resource);
    }

    /**
     * Marque le moyen comme étant de retour au CRM
     *
     * @param intervId L'id de l'intervention
     * @param resId    L'id du moyen
     */
    public void moveResourceToCrm(Long intervId, Long resId) {
        Intervention interv = intervRepository.getOne(intervId);
        Resource resource = resourceRepository.getOne(resId);
        checkInterventionHasPoint(interv, resource);

        boolean ended = asList(MacroState.FINAL_STATES).contains(resource.getMacroState());
        if (ended)
            throw new IllegalStateException("Resource " + resId + " could not be moved back to CRM.");

        resource.setLatitude(null);
        resource.setLongitude(null);

        if (WAITING.equals(resource.getMacroState()))
            resource.setMicroState(MOVING);
        else
            resource.setMicroState(IDLE);

        resourceRepository.save(resource);
    }

    /**
     * Change le rôle du moyen
     *
     * @param intervId L'id de l'intervention
     * @param resId    L'id du moyen
     * @param role     Le nouveau rôle du moyen
     */
    public void changeResourceRole(Long intervId, Long resId, int role) {
        Intervention interv = intervRepository.getOne(intervId);
        Resource resource = resourceRepository.getOne(resId);
        checkInterventionHasPoint(interv, resource);

        resource.setRole(role);
        resourceRepository.save(resource);
    }

    /**
     * Vérifie que le point (POI / moyens) fait bien partie de cette intervention.
     *
     * @param interv L'intervention
     * @param point  Le point
     */
    private void checkInterventionHasPoint(Intervention interv, AbstractMapPoint point) {
        if (!Objects.equals(interv, point.getIntervention())) {
            throw new EntityNotFoundException("Intervention " + interv.getId() + " has no Resource with id = " + point.getId());
        }
    }
}
