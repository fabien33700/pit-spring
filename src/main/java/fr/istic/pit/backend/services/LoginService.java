package fr.istic.pit.backend.services;

import fr.istic.pit.backend.model.entities.User;
import fr.istic.pit.backend.repositories.UserRepository;
import org.springframework.stereotype.Service;

/**
 * Service pour la connexion des utilisateurs à l'applications
 */
@Service
public class LoginService {

    /**
     * Repository des utilisateurs
     */
    private final UserRepository userRepository;

    /**
     * Constructeur du service
     *
     * @param userRepository Repository des utilisateurs
     */
    public LoginService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    /**
     * Gestion de connexion de l'application.
     *
     * @param user les informations de l'utilisateur reçu depuis la tablette
     * @return true si les informations correspondent à un utilisateur, false sinon
     */
    public boolean login(User user) {
        User bddUser = this.userRepository.findByLogin(user.getLogin());
        return bddUser != null && bddUser.getPassword().equals(user.getPassword());
    }
}
