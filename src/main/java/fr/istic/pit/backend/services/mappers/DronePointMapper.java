package fr.istic.pit.backend.services.mappers;

import fr.istic.pit.backend.model.dto.DronePointDTO;
import fr.istic.pit.backend.model.entities.DronePoint;
import org.springframework.stereotype.Service;

/**
 * Mapper pour les points de mission de drone.
 */
@Service
public class DronePointMapper implements Mapper<DronePoint, DronePointDTO> {

    @Override
    public DronePointDTO map(DronePoint source) {
        DronePointDTO dto = new DronePointDTO();
        dto.id = source.getId();
        dto.altitude = source.getAltitude();
        dto.latitude = source.getLatitude();
        dto.longitude = source.getLongitude();
        dto.orderNumber = source.getOrderNumber();
        return dto;
    }

    @Override
    public DronePoint reverseMap(DronePointDTO target) {
        return new DronePoint.Builder()
            .altitude(target.altitude)
            .latitude(target.latitude)
            .longitude(target.longitude)
            .orderNumber(target.orderNumber)
            .build();
    }
}
