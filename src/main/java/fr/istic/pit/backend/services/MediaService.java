package fr.istic.pit.backend.services;

import fr.istic.pit.backend.repositories.InterventionRepository;
import org.apache.commons.io.IOUtils;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.io.*;
import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.toList;

/**
 * Service pour la gestion des médias (photos et vidéos)
 */
@Service
public class MediaService {

    /**
     * Chemin par défaut du dossier où stocker les médias
     */
    private final static String BASE_MEDIA_PATH = "/data";

    /**
     * Repository des interventions
     */
    private final InterventionRepository intervRepository;

    /**
     * Constructeur du service
     *
     * @param intervRepository Repository des interventions
     */
    public MediaService(InterventionRepository intervRepository) {
        this.intervRepository = intervRepository;
    }


    /**
     * Sauvegarde une photo sur le serveur.
     *
     * @param intervId  L'id de l'intervention
     * @param latitude  La latitude du lieu où a été pris la photo
     * @param longitude La longitude du lieu où a été pris la photo
     * @param photo     La photo, encodé en Base64
     */
    @SuppressWarnings("ResultOfMethodCallIgnored")
    public void savePhoto(Long intervId, String latitude, String longitude, String photo) {
        // Vérifie si l'intervention existe
        intervRepository.getOne(intervId);

        File directory = new File(pathPhoto(intervId, latitude, longitude));
        if (!directory.exists())
            directory.mkdirs();
        String name = safeString(LocalDateTime.now().toString()).replace('T', '_') + ".jpg";
        System.out.println(name);
        writeImage(pathPhoto(intervId, latitude, longitude, name), photo);
    }

    /**
     * Récupère la liste des noms de photos pour une intervention et une position donnée.
     *
     * @param intervId  L'id de l'intervention
     * @param latitude  La latitude du lieu
     * @param longitude La longitude du lieu
     * @return La liste des noms
     */
    public List<String> getListPhotoName(Long intervId, String latitude, String longitude) {
        // Vérifie si l'intervention existe
        intervRepository.getOne(intervId);

        File directory = new File(pathPhoto(intervId, latitude, longitude));
        File[] files = directory.listFiles();
        if (files != null)
            return Arrays.stream(files).filter(File::isFile).map(File::getName).map(name -> name.substring(0, name.length() - 4)).collect(toList());
        else
            return Collections.emptyList();
    }

    /**
     * Récupère une photo du serveur.
     *
     * @param intervId  L'id de l'intervention
     * @param latitude  La latitude du lieu
     * @param longitude La longitude du lieu
     * @param name      Le nom de la photo
     * @return la photo encodée en base64
     */
    public String getPhoto(Long intervId, String latitude, String longitude, String name) {
        // Vérifie si l'intervention existe
        intervRepository.getOne(intervId);

        return readImage(pathPhoto(intervId, latitude, longitude, name + ".jpg"));
    }

    /**
     * Sauvegarde la dernière frame de la vidéo sur le serveur.
     *
     * @param intervId L'id de l'intervention
     * @param frame    La frame, encodé en Base64
     */
    @SuppressWarnings({"ResultOfMethodCallIgnored"})
    public void uploadVideoFrame(Long intervId, String frame) {
        // Vérifie si l'intervention existe
        intervRepository.getOne(intervId);

        File directory = new File(BASE_MEDIA_PATH + "/" + intervId + "/videos");
        if (!directory.exists())
            directory.mkdirs();
        String name = safeString(LocalDateTime.now().toString()).replace('T', '_') + ".jpg";
        writeImage(BASE_MEDIA_PATH + "/" + intervId + "/videos/" + name, frame);
    }

    /**
     * Renvoie la dernière frame de la vidéo sur le serveur.
     *
     * @param intervId L'id de l'intervention
     * @return La frame, encodé en Base64
     */
    public String getLastVideoFrame(Long intervId) {
        if (!intervRepository.existsById(intervId))
            throw new EntityNotFoundException("No intervention with id = " + intervId);

        File directory = new File(BASE_MEDIA_PATH + "/" + intervId + "/videos");
        File[] files = directory.listFiles();
        List<String> frames = new LinkedList<>();
        if (files != null)
            frames.addAll(Arrays.stream(files)
                .filter(File::isFile)
                .map(File::getName)
                .collect(Collectors.toList()));

        if (frames.isEmpty())
            throw new EntityNotFoundException("Aucune frame n'a été trouvé.");

        frames.sort(Collections.reverseOrder());
        return readImage(BASE_MEDIA_PATH + "/" + intervId + "/videos/" + frames.get(0));
    }

    /**
     * Écrit une image dans le système de fichier.
     *
     * @param path    le chemin de l'image
     * @param dataB64 l'image encodé en Base64
     */
    private void writeImage(String path, String dataB64) {
        byte[] data = Base64.getDecoder().decode(dataB64);
        try (OutputStream stream = new FileOutputStream(path)) {
            stream.write(data);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Lit une image dans le système de fichier
     *
     * @param path le chemin de l'image
     * @return l'image encodé en Base64
     */
    private String readImage(String path) {
        try (InputStream stream = new FileInputStream(path)) {
            return Base64.getEncoder().encodeToString(IOUtils.toByteArray(stream));
        } catch (IOException e) {
            throw new EntityNotFoundException("L'image demandé n'existe pas.");
        }
    }

    /**
     * Remplace les caractères non autorisés dans le système de fichier.
     *
     * @param string la chaine de caractère
     * @return la chaine de caractère au bon format
     */
    private String safeString(String string) {
        return string.replaceAll("[-:.]", "_");
    }

    /**
     * Renvoie le chemin vers le dossier de photo.
     *
     * @param intervId  L'id de l'intervention
     * @param latitude  La latitude du lieu
     * @param longitude La longitude du lieu
     * @return le chemin
     */
    private String pathPhoto(Long intervId, String latitude, String longitude) {
        return pathPhoto(intervId, latitude, longitude, null);
    }

    /**
     * Renvoie le chemin vers une photo.
     *
     * @param intervId  L'id de l'intervention
     * @param latitude  La latitude du lieu
     * @param longitude La longitude du lieu
     * @param name      le nom de la photo
     * @return le chemin
     */
    private String pathPhoto(Long intervId, String latitude, String longitude, String name) {
        return BASE_MEDIA_PATH + "/" + intervId + "/" + safeString(latitude) + "/" + safeString(longitude) + (name != null ? "/" + name : "");
    }
}
