package fr.istic.pit.backend.controllers;

import fr.istic.pit.backend.model.dto.ResourceDTO;
import fr.istic.pit.backend.services.ResourceService;
import fr.istic.pit.backend.services.mappers.ResourceMapper;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

import static fr.istic.pit.backend.controllers.ResourceController.ENDPOINT;

/**
 * Contrôleur pour la recherche de moyens indépendamment de leurs interventions.
 */
@RestController
@RequestMapping(ENDPOINT)
@Tag(name = ENDPOINT, description = "Le point de terminaison pour la gestion des ressources")
public class ResourceController {
    /**
     * URI de base du contrôleur
     */
    public static final String ENDPOINT = "/resources";

    /**
     * Service de gestion des moyens
     */
    private final ResourceService resourceService;

    /**
     * Mapper de moyen
     */
    private final ResourceMapper resourceMapper;

    /**
     * Constructeur du contrôleur
     *
     * @param resourceService Service de gestion des moyens
     * @param resourceMapper  Mapper de moyen
     */
    public ResourceController(ResourceService resourceService,
                              ResourceMapper resourceMapper) {
        this.resourceService = resourceService;
        this.resourceMapper = resourceMapper;
    }

    /**
     * Recherche et récupère les moyens correspondant à l'état macro et micro recherché,
     * ou tous les moyens si aucun critère d'état n'est spécifié.
     *
     * @param macro L'état macro recherché, optionnel
     * @param micro L'état micro recherché, optionnel
     * @return La réponse de l'API, la liste des moyens trouvés
     */
    @GetMapping
    @Operation(description = "**Récupère** tous les moyens de toutes les interventions possédant l'état recherché." +
        "*Les critères de recherches sont optionnels. Ne pas fournir de critères affiche tous les moyens*.")
    public List<ResourceDTO> searchByState(@RequestParam(required = false)
                                           @Parameter(description = "L'état macro des moyens à rechercher") String macro,
                                           @RequestParam(required = false)
                                           @Parameter(description = "L'état micro des moyens à rechercher") String micro) {
        return resourceMapper.mapList(
            resourceService.getAllByState(macro, micro));
    }
}
