package fr.istic.pit.backend.controllers;

import fr.istic.pit.backend.model.dto.MissionCreationDTO;
import fr.istic.pit.backend.model.dto.MissionDTO;
import fr.istic.pit.backend.model.entities.Mission;
import fr.istic.pit.backend.services.MediaService;
import fr.istic.pit.backend.services.mappers.DronePointMapper;
import fr.istic.pit.backend.services.mappers.MissionMapper;
import fr.istic.pit.backend.services.mqtt.drone.DroneService;
import fr.istic.pit.backend.services.mqtt.sync.EmitUpdateEvent;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;

import javax.validation.Valid;
import java.net.URI;
import java.util.List;
import java.util.Optional;

import static fr.istic.pit.backend.controllers.DroneController.ENDPOINT;
import static fr.istic.pit.backend.services.mqtt.drone.DroneService.AfterStopAction.DO_NOTHING;
import static fr.istic.pit.backend.services.mqtt.drone.DroneService.AfterStopAction.SEND_RTL;
import static fr.istic.pit.backend.services.mqtt.sync.model.UpdateEventType.MISSION_CREATE;
import static fr.istic.pit.backend.services.mqtt.sync.model.UpdateEventType.REGISTERED_DRONE;

/**
 * Contrôleur pour les routes du drone
 */
@RestController
@RequestMapping(ENDPOINT)
@Tag(name = ENDPOINT, description = "Le point de terminaison pour la gestion du drone")
@SuppressWarnings("rawtypes")
public class DroneController {

    /**
     * URI de base du contrôleur
     */
    public static final String ENDPOINT = "/interventions/{intervId}/drone";

    /**
     * Service de gestion du drone
     */
    private final DroneService droneService;

    /**
     * Mapper pour les points de mission du drone
     */
    private final DronePointMapper dronePointMapper;

    /**
     * Mapper pour les missions
     */
    private final MissionMapper missionMapper;

    /**
     * Service de gestion des médias
     */
    private final MediaService mediaService;

    /**
     * Constructeur du contrôleur
     *
     * @param droneService     Service de gestion du drone
     * @param dronePointMapper Mapper pour les points de mission du drone
     * @param missionMapper    Mapper pour les missions
     * @param mediaService     Service de gestion des médias
     */
    public DroneController(DroneService droneService,
                           DronePointMapper dronePointMapper,
                           MissionMapper missionMapper,
                           MediaService mediaService) {
        this.droneService = droneService;
        this.dronePointMapper = dronePointMapper;
        this.missionMapper = missionMapper;
        this.mediaService = mediaService;
    }

    /**
     * Route pour l'enregistrement d'un drone auprès de l'intervention.
     *
     * @param intervId L'id de l'intervention
     * @return Réponse de l'API
     */
    @PostMapping("/register")
    @Operation(
        description = "**Enregistre** un drone auprès d'une *intervention*.",
        responses = {
            @ApiResponse(responseCode = "204", description = "Le drone est bien enregistré auprès de l'intervention"),
            @ApiResponse(responseCode = "400", description = "L'intervention n'existe pas"),
            @ApiResponse(responseCode = "409", description = "Une mission est déjà enregistrée pour cette intervention")
        })
    @EmitUpdateEvent(REGISTERED_DRONE)
    public ResponseEntity registerDrone(@PathVariable @Parameter(description = "L'id de l'intervention") Long intervId) {
        droneService.registerDrone(intervId);
        return ResponseEntity.noContent().build();
    }

    /**
     * Route pour la planification d'une nouvelle mission.
     *
     * @param intervId L'id de l'intervention
     * @param mission  L'objet contenant la mission
     * @param builder  Dépendance externe (injectée par Spring) permettant de construire l'URI de l'objet nouvellement créé
     * @return La réponse de l'API
     */
    @PostMapping("/mission")
    @Operation(
        description = "**Enregistre** un drone auprès d'une *intervention*.",
        responses = {
            @ApiResponse(responseCode = "201", description = "Le drone est bien enregistré auprès de l'intervention"),
            @ApiResponse(responseCode = "400", description = "L'intervention n'existe pas"),
            @ApiResponse(responseCode = "409", description = "Aucun drone n'a été enregistré sur l'intervention")
        })
    @EmitUpdateEvent(MISSION_CREATE)
    public ResponseEntity createMission(@PathVariable @Parameter(description = "L'id de l'intervention") Long intervId,
                                        @Parameter(description = "Les informations nécessaires à la création de la mission")
                                        @Valid @RequestBody MissionCreationDTO mission,
                                        UriComponentsBuilder builder) {
        Mission created = droneService.createMission(intervId, mission.missionLoop,
            dronePointMapper.reverseMapList(mission.points));
        URI resourceUri = builder.path(ENDPOINT + "/missions/{id}")
            .buildAndExpand(intervId, created.getId())
            .toUri();

        return ResponseEntity.created(resourceUri).build();
    }

    /**
     * Route permettant de récupérer la dernière mission/mission en cours
     *
     * @param intervId L'id de l'intervention
     * @return La réponse de l'API
     */
    @GetMapping("/mission")
    @Operation(description = "**Affiche** la dernière mission de l'intervention (en cours ou non).")
    public ResponseEntity<MissionDTO> getMission(@PathVariable @Parameter(description = "L'id de l'intervention") Long intervId) {
        Optional<MissionDTO> mission = droneService.getCurrentMission(intervId)
            .map(missionMapper::map);
        mission.ifPresent(m -> m.lastDroneState = droneService.getLastDroneState());
        return mission.map(ResponseEntity::ok)
            .orElse(ResponseEntity.noContent().build());
    }

    /**
     * Route permettant d'arrêter la mission de l'intervention.
     *
     * @param intervId L'id de l'intervention
     * @return La réponse de l'API
     */
    @PatchMapping(value = "/mission/stop")
    @Operation(description = "**Stoppe** la mission en cours pour l'intervention.")
    public ResponseEntity<?> stopMission(@PathVariable @Parameter(description = "L'id de l'intervention") Long intervId) {
        droneService.stopMission(intervId, DO_NOTHING);
        return ResponseEntity.noContent().build();
    }

    /**
     * Route permettant d'arrêter la mission de l'intervention et
     * de demander au drone d'atterir
     *
     * @param intervId L'id de l'intervention
     * @return La réponse de l'API
     */
    @PatchMapping(value = "/mission/rtl")
    @Operation(description = "Envoi l'ordre au drone de retourner à son point de lancement.<br/>" +
        "*Si une mission était en cours, elle sera automatiquement annulée.*")
    public ResponseEntity<?> sendRTL(@PathVariable @Parameter(description = "L'id de l'intervention") Long intervId) {
        droneService.stopMission(intervId, SEND_RTL);
        return ResponseEntity.noContent().build();
    }

    /**
     * Route permettant au drone d'envoyer la photo prise au serveur.
     *
     * @param intervId  L'id de l'intervention
     * @param latitude  La latitude du point de prise de vue
     * @param longitude La longitude du point de prise de vue
     * @param photo     La photo, encodée en base64
     * @return La réponse de l'API
     */
    @PostMapping("/photo")
    @Operation(description = "Upload une photo")
    public ResponseEntity uploadPhoto(@PathVariable @Parameter(description = "L'identifiant de l'intervention") Long intervId,
                                      @RequestParam @Parameter(description = "La latitude du lieu où a été pris la photo") String latitude,
                                      @RequestParam @Parameter(description = "La longitude du lieu où a été pris la photo") String longitude,
                                      @RequestBody @Parameter(description = "La photo, encodé en Base64") String photo) {
        mediaService.savePhoto(intervId, latitude, longitude, photo);
        return ResponseEntity.status(HttpStatus.CREATED).build();
    }

    /**
     * Route permettant de récupérer la liste des noms de photos.
     *
     * @param intervId  L'id de l'intervention
     * @param latitude  La latitude du point de prise de vue
     * @param longitude La longitude du point de prise de vue
     * @return La réponse de l'API
     */
    @GetMapping("/photo")
    @Operation(description = "Récupère la liste des noms de photo")
    public List<String> getListPhotoName(@PathVariable @Parameter(description = "L'identifiant de l'intervention") Long intervId,
                                         @RequestParam @Parameter(description = "La latitude du lieu où a été pris la photo") String latitude,
                                         @RequestParam @Parameter(description = "La longitude du lieu où a été pris la photo") String longitude) {
        return mediaService.getListPhotoName(intervId, latitude, longitude);
    }

    /**
     * Route permettant de récupérer une photo.
     *
     * @param intervId  L'id de l'intervention
     * @param latitude  La latitude du point de prise de vue
     * @param longitude La longitude du point de prise de vue
     * @param photoName Le nom de la photo
     * @return La réponse de l'API
     */
    @GetMapping("/photo/{photoName}")
    @Operation(description = "Récupère une photo")
    public String getPhoto(@PathVariable @Parameter(description = "L'identifiant de l'intervention") Long intervId,
                           @RequestParam @Parameter(description = "La latitude du lieu où a été pris la photo") String latitude,
                           @RequestParam @Parameter(description = "La longitude du lieu où a été pris la photo") String longitude,
                           @PathVariable @Parameter(description = "Le nom de la photo") String photoName) {
        return mediaService.getPhoto(intervId, latitude, longitude, photoName);
    }

    /**
     * Route permettant au drone d'envoyer la dernière frame de la vidéo temps réel au serveur.
     *
     * @param intervId L'id de l'intervention
     * @param frame    L'image de la vidéo, encodée en base64
     * @return La réponse de l'API
     */
    @PostMapping("/video")
    @Operation(description = "Upload la dernière frame de la vidéo")
    public ResponseEntity uploadVideo(@PathVariable @Parameter(description = "L'identifiant de l'intervention") Long intervId,
                                      @RequestBody @Parameter(description = "La frame de la vidéo, encodé en Base64") String frame) {
        mediaService.uploadVideoFrame(intervId, frame);
        return ResponseEntity.status(HttpStatus.ACCEPTED).build();
    }

    /**
     * Route permettant de récupérer la dernière frame de la vidéo.
     *
     * @param intervId L'id de l'intervention
     * @return La réponse de l'API
     */
    @GetMapping("/video")
    @Operation(description = "Récupère la dernière frame de la vidéo")
    public String getVideo(@PathVariable @Parameter(description = "L'identifiant de l'intervention") Long intervId) {
        return mediaService.getLastVideoFrame(intervId);
    }
}
