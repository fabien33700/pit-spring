package fr.istic.pit.backend.controllers.errors;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.UUID;

/**
 * DTO des réponses d'erreurs de l'API
 */
@JsonInclude(Include.NON_EMPTY)
public class ErrorResponse {
    /**
     * La clé d'en-tête de l'identifiant unique de l'erreur
     */
    private static final String HEADER_ERROR_UNIQUE_ID = "X-PIT-Error-UniqueId";

    /**
     * Identifiant unique de l'erreur, pour suivi dans les logs
     */
    @JsonIgnore
    public final String id;

    /**
     * Code de retour HTTP de l'erreur
     */
    public int status;

    /**
     * Statut de retour
     */
    public String reason;

    /**
     * Détails supplémentaires de l'erreur
     */
    public Object details;

    /**
     * Constructeur de base
     */
    public ErrorResponse() {
        this.id = UUID.randomUUID().toString();
    }

    /**
     * Définit le statut de l'erreur.
     *
     * @param status Le statut
     * @return L'instance de la réponse d'erreur
     */
    public ErrorResponse status(HttpStatus status) {
        this.status = status.value();
        return this;
    }

    /**
     * Définit la raison de l'erreur.
     *
     * @param reason La raison
     * @return L'instance de la réponse d'erreur
     */
    public ErrorResponse reason(String reason) {
        this.reason = reason;
        return this;
    }

    /**
     * Ajoute des détails à l'erreur.
     *
     * @param details Les détails supplémentaires
     * @return L'instance de la réponse d'erreur
     */
    public ErrorResponse details(Object details) {
        this.details = details;
        return this;
    }

    /**
     * Transforme l'erreur en une réponse pouvant être renvoyée au client.
     *
     * @return Une instance de ResponseEntity
     */
    public ResponseEntity<ErrorResponse> toResponse() {
        return ResponseEntity
            .status(status)
            .header(HEADER_ERROR_UNIQUE_ID, id)
            .body(this);
    }
}
