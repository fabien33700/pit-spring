package fr.istic.pit.backend.controllers.errors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;
import org.springframework.web.server.MethodNotAllowedException;

import javax.persistence.EntityNotFoundException;
import java.time.format.DateTimeParseException;

/**
 * Gestionnaire d'erreur de l'API Rest
 */
@ControllerAdvice
public class RestExceptionHandler {
    private static final Logger LOGGER = LoggerFactory.getLogger(RestExceptionHandler.class);

    /**
     * Résout le code de retour HTTP en fonction du type d'erreur survenue.
     *
     * @param root L'exception racine de l'erreur
     * @return Le statut HTTP de la réponse d'erreur
     */
    private HttpStatus getStatusForException(Exception root) {
        try {
            throw root;
        } catch (EntityNotFoundException ex) {
            return HttpStatus.NOT_FOUND;
        } catch (DuplicateKeyException | IllegalStateException ex) {
            return HttpStatus.CONFLICT;
        } catch (MethodNotAllowedException | HttpRequestMethodNotSupportedException ex) {
            return HttpStatus.METHOD_NOT_ALLOWED;
        } catch (MethodArgumentNotValidException | HttpMessageNotReadableException | IllegalArgumentException |
            MethodArgumentTypeMismatchException | DateTimeParseException | MissingServletRequestParameterException ex) {
            return HttpStatus.BAD_REQUEST;
        } catch (UnauthorizedException ex) {
            return HttpStatus.UNAUTHORIZED;
        } catch (UnsupportedOperationException ex) {
            return HttpStatus.NOT_IMPLEMENTED;
        } catch (Exception ex) {
            return HttpStatus.INTERNAL_SERVER_ERROR;
        }
    }

    /**
     * Gestion des erreurs de validation (JSR-303)
     *
     * @param ex L'exception attrapée par Spring
     * @return La réponse d'erreur qui sera renvoyée au client
     * @see javax.validation
     * @see MethodArgumentNotValidException
     */
    @ExceptionHandler(MethodArgumentNotValidException.class)
    public ResponseEntity<ErrorResponse> handleValidationException(MethodArgumentNotValidException ex) {
        // Récupération des informations sur le problème de validation
        ValidationErrorDetails details = new ValidationErrorDetails(ex);

        // Construction de la réponse d'ereur
        HttpStatus status = getStatusForException(ex);
        ErrorResponse error = new ErrorResponse()
            .status(status)
            .reason("The request body validation had failed")
            .details(details.fieldErrors);

        LOGGER.warn("{} {} {} : {}\n {}", status.value(), status.getReasonPhrase(),
            error.id, ex.getMessage(), details.toString());

        return error.toResponse();
    }

    /**
     * Gestion générique des autres types d'exceptions
     *
     * @param ex L'exception attrapée par Spring
     * @return La réponse d'erreur qui sera renvoyée au client
     */
    @ExceptionHandler(Exception.class)
    public ResponseEntity<ErrorResponse> handleException(Exception ex) {
        // Résolution du statut
        HttpStatus status = getStatusForException(ex);

        // Construction de la réponse d'erreur
        ErrorResponse error = new ErrorResponse()
            .status(status)
            .reason(ex.getMessage());

        /* S'il s'agit d'une erreur serveur, on logue la stacktrace avec l'identifiant,
         * ce qui permettra à l'administrateur de retrouver la stacktrace correspondant
         * à une erreur survenue pour un client
         */
        if (status.value() >= 500)
            LOGGER.error("Server exception has occurred : " + error.id, ex);

        LOGGER.warn("{} {} {} : {}", status.value(), status.getReasonPhrase(),
            error.id, ex.getMessage());

        return error.toResponse();
    }
}
