package fr.istic.pit.backend.controllers.errors;

/**
 * Exception levée lorsque l'utilisateur n'est pas autorisé à accéder à la ressource.
 * Correspond à l'erreur HTTP 401
 */
public class UnauthorizedException extends RuntimeException {

    /**
     * Constructeur
     */
    public UnauthorizedException() {
    }

    /**
     * Constructeur avec message
     *
     * @param message Le message d'erreur
     */
    public UnauthorizedException(String message) {
        super(message);
    }
}
