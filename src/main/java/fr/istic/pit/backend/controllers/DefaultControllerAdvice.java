package fr.istic.pit.backend.controllers;

import org.springframework.validation.DataBinder;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.InitBinder;

/**
 * Classe permettant de personnaliser le comportement de tous les contrôleurs
 * de l'application
 */
@ControllerAdvice
public class DefaultControllerAdvice {

    @InitBinder
    private void setFieldAccess(DataBinder binder) {
        // Permet à la validation (JSR-303) d'accéder aux champs des DTO directement
        binder.initDirectFieldAccess();
    }
}
