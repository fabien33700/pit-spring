package fr.istic.pit.backend.controllers;

import fr.istic.pit.backend.controllers.errors.UnauthorizedException;
import fr.istic.pit.backend.model.dto.*;
import fr.istic.pit.backend.model.entities.Intervention;
import fr.istic.pit.backend.model.entities.PointOfInterest;
import fr.istic.pit.backend.model.entities.Resource;
import fr.istic.pit.backend.services.InterventionService;
import fr.istic.pit.backend.services.mappers.InterventionMapper;
import fr.istic.pit.backend.services.mappers.PointOfInterestMapper;
import fr.istic.pit.backend.services.mappers.ResourceMapper;
import fr.istic.pit.backend.services.mqtt.sync.EmitUpdateEvent;
import fr.istic.pit.backend.services.security.RoleService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;

import javax.persistence.EntityNotFoundException;
import javax.validation.Valid;
import java.net.URI;
import java.util.List;

import static fr.istic.pit.backend.controllers.InterventionController.ENDPOINT;
import static fr.istic.pit.backend.services.mqtt.sync.model.UpdateEventType.*;

/**
 * Contrôleur pour la gestion des interventions et de leurs points d'intérêts et moyens.
 */
@RestController
@RequestMapping(ENDPOINT)
@Tag(name = ENDPOINT, description = "Le point de terminaison pour la gestion des interventions")
@SuppressWarnings("rawtypes")
public class InterventionController {

    /**
     * URI de base du contrôleur
     */
    public static final String ENDPOINT = "/interventions";

    /**
     * Message d'erreur pour le rôle CODIS
     */
    public static final String REQUIRE_CODIS = "Only CODIS can perform this operation.";

    /**
     * Service de gestion des interventions
     */
    private final InterventionService service;

    /**
     * Mapper d'intervention
     */
    private final InterventionMapper intervMapper;

    /**
     * Mapper de moyen
     */
    private final ResourceMapper resourceMapper;

    /**
     * Mapper de points d'intérêt
     */
    private final PointOfInterestMapper pointOfInterestMapper;

    /**
     * Service d'identification du rôle courant
     */
    private final RoleService roleService;

    /**
     * Constructeur du contrôleur
     *
     * @param service               Service de gestion des interventions
     * @param intervMapper          Mapper d'intervention
     * @param resourceMapper        Mapper de moyen
     * @param pointOfInterestMapper Mapper de points d'intérêt
     * @param roleService           Service d'identification du rôle courant
     */
    public InterventionController(InterventionService service,
                                  InterventionMapper intervMapper,
                                  ResourceMapper resourceMapper,
                                  PointOfInterestMapper pointOfInterestMapper,
                                  RoleService roleService) {
        this.service = service;
        this.intervMapper = intervMapper;
        this.resourceMapper = resourceMapper;
        this.pointOfInterestMapper = pointOfInterestMapper;
        this.roleService = roleService;
    }

    /**
     * Route permettant de récupérer une intervention.
     *
     * @param id L'id de l'intervention
     * @return La réponse de l'API
     */
    @GetMapping("/{id}")
    @Operation(description = "**Retourne** l'intervention demandée, si elle existe.")
    public InterventionDTO getIntervention(@PathVariable
                                           @Parameter(description = "L'id de l'intervention à récupérer") Long id) {
        return service.findById(id)
            .map(intervMapper::map)
            .orElseThrow(() -> new EntityNotFoundException("Intervention " + id + " not found"));
    }

    /**
     * Route permettant de récupérer toutes les interventions
     *
     * @return La réponse de l'API
     */
    @GetMapping
    @Operation(description = "**Retourne** toutes les interventions.")
    public List<InterventionDTO> getInterventions() {
        return intervMapper.mapList(service.getAll());
    }

    /**
     * Route permettant de créer une intervention.
     * Émet une notification de mise à jour de type <code>INTERV_CREATE</code> si l'opération a réussi.
     *
     * @param intervention L'objet contenant l'intervention à créer
     * @param builder      Dépendance externe (injectée par Spring) permettant de construire l'URI de l'objet nouvellement créé
     * @return La réponse de l'API
     */
    @PostMapping
    @Operation(description = "**Créé** une intervention.")
    @EmitUpdateEvent(INTERV_CREATE)
    public ResponseEntity createIntervention(@Valid @RequestBody @Parameter(description = "L'intervention à créer")
                                                 InterventionDTO intervention,
                                             UriComponentsBuilder builder) {
        Intervention created = service.create(intervMapper.reverseMap(intervention));
        URI resourceUri = builder.path(ENDPOINT + "/{id}")
            .buildAndExpand(created.getId())
            .toUri();

        return ResponseEntity.created(resourceUri).build();
    }

    /**
     * Route permettant de modifier l'intervention.
     *
     * @param id      L'id de l'intervention
     * @param updates Les modifications à apporter
     * @return La réponse de l'API
     */
    @PutMapping("/{id}")
    @Operation(description = "**Met à jour** une intervention.\n" +
        "*Utiliser les opérations dédiées aux POI/moyens (/pointsOfInterest et /resources) pour modifier ceux de l'intervention.")
    public InterventionDTO modifyIntervention(@PathVariable
                                              @Parameter(description = "L'identifiant de l'intervention à modifier") Long id,
                                              @Valid @RequestBody
                                              @Parameter(description = "Les modifications à apporter") InterventionDTO updates) {
        return intervMapper.map(service.update(id, intervMapper.reverseMap(updates)));
    }

    /**
     * Route permettant de clôturer une intervention.
     * Émet une notification de mise à jour de type <code>INTERV_DISABLE</code> si l'opération a réussi.
     *
     * @param id L'id de l'intervention
     * @return La réponse de l'API
     */
    @DeleteMapping("/{id}")
    @Operation(description = "**Clôture** une intervention.")
    @EmitUpdateEvent(INTERV_DISABLE)
    public ResponseEntity deleteIntervention(@PathVariable
                                             @Parameter(description = "L'identifiant de l'intervention à clôturer") Long id) {
        service.delete(id);
        return ResponseEntity.noContent().build();
    }

    /**
     * Afficher les POI d'une intervention
     *
     * @param intervId L'id de l'intervention
     * @return La liste des points d'intérêt de l'intervention demandée
     */
    @GetMapping("/{intervId}/pointsOfInterest")
    @Operation(description = "**Récupère** les points d'intérêts de l'intervention active demandée.")
    public List<PointOfInterestDTO> getInterventionPOIs(@PathVariable
                                                        @Parameter(description = "L'id de l'intervention") Long intervId) {
        return service.findById(intervId)
            .map(Intervention::getPointsOfInterest)
            .map(pointOfInterestMapper::mapList)
            .orElseThrow(() -> new EntityNotFoundException("Intervention " + intervId + " not found"));
    }

    /**
     * Afficher les moyens d'une intervention
     *
     * @param intervId L'id de l'intervention
     * @return La liste des moyens de l'intervention demandée
     */
    @GetMapping("/{intervId}/resources")
    @Operation(description = "**Récupère** les moyens de l'intervention active demandée.")
    public List<ResourceDTO> getInterventionResources(@PathVariable
                                                      @Parameter(description = "L'id de l'intervention") Long intervId) {
        return service.findById(intervId)
            .map(Intervention::getResources)
            .map(resourceMapper::mapList)
            .orElseThrow(() -> new EntityNotFoundException("Intervention " + intervId + " not found"));
    }

    /**
     * Récupère un POI d'une intervention
     *
     * @param intervId L'id de l'intervention
     * @param poiId    L'id du point d'intérêt
     * @return Le point d'intérêt demandé
     */
    @GetMapping("/{intervId}/pointsOfInterest/{poiId}")
    @Operation(description = "**Récupère** le point d'intérêt de l'intervention active demandée.")
    public PointOfInterestDTO getInterventionPOI(@PathVariable @Parameter(description = "L'id de l'intervention") Long intervId,
                                                 @PathVariable @Parameter(description = "L'id du POI") Long poiId) {
        return pointOfInterestMapper.map(
            service.findPOIInIntervention(intervId, poiId));
    }

    /**
     * Récupère un moyen d'une intervention
     *
     * @param intervId L'id de l'intervention
     * @param resId    L'id du moyen
     * @return Le moyen demandé
     */
    @GetMapping("/{intervId}/resources/{resId}")
    @Operation(description = "**Récupère** le moyen de l'intervention active demandée.")
    public ResourceDTO getInterventionResources(@PathVariable @Parameter(description = "L'id de l'intervention") Long intervId,
                                                @PathVariable @Parameter(description = "L'id du moyen") Long resId) {
        return resourceMapper.map(
            service.findResourceInIntervention(intervId, resId));
    }

    /**
     * Route permettant de supprimer un point d'intérêt.
     * Émet une notification de mise à jour de type <code>POI_DISABLE</code> si l'opération a réussi.
     *
     * @param intervId L'id de l'intervention
     * @param poiId    L'id du point d'intérêt
     * @return La réponse de l'API
     */
    @DeleteMapping("/{intervId}/pointsOfInterest/{poiId}")
    @Operation(description = "**Retire** un *POI* d'une intervention. \n*Note : le point est archivé et non supprimé*")
    @EmitUpdateEvent(POI_DISABLE)
    public ResponseEntity deletePointOfInterest(@PathVariable @Parameter(description = "L'identifiant de l'intervention") Long intervId,
                                                @PathVariable @Parameter(description = "L'identifiant du POI à retirer") Long poiId) {
        service.removePointOfInterest(intervId, poiId);
        return ResponseEntity.noContent().build();
    }

    /**
     * Route permettant d'ajouter un point d'intérêt à une intervention.
     * Émet une notification de mise à jour de type <code>POI_CREATE</code> si l'opération a réussi.
     *
     * @param intervId        L'id de l'intervention
     * @param pointOfInterest Le point d'intérêt à ajouter
     * @param builder         Permet de construire l'URL du moyen créé (injecté par Spring)
     * @return La réponse de l'API
     */
    @PostMapping("/{intervId}/pointsOfInterest")
    @Operation(description = "**Ajoute** un *POI* à une intervention.")
    @EmitUpdateEvent(POI_CREATE)
    public ResponseEntity addPointOfInterest(@PathVariable @Parameter(description = "L'identifiant de l'intervention") Long intervId,
                                             @RequestBody @Parameter(description = "Le POI à ajouter") @Valid PointOfInterestDTO pointOfInterest,
                                             UriComponentsBuilder builder) {
        PointOfInterest newer = pointOfInterestMapper.reverseMap(pointOfInterest);
        PointOfInterest created = service.addPointOfInterest(intervId, newer);
        URI resourceUri = builder.path(ENDPOINT + "/{id}/pointsOfInterest/{poiId}")
            .buildAndExpand(intervId, created.getId())
            .toUri();

        return ResponseEntity.created(resourceUri).build();
    }

    /**
     * Route permettant de créer une demande de moyen pour une intervention.
     * Émet une notification de mise à jour de type <code>RESOURCE_REQUEST_CREATE</code> si l'opération a réussi.
     *
     * @param intervId L'id de l'intervention
     * @param resource Le moyen à demander
     * @param builder  Permet de construire l'URL du moyen créé (injecté par Spring)
     * @return La réponse de l'API
     */
    @PostMapping("/{intervId}/resources")
    @Operation(description = "**Création** d'une demande de *moyen* par le COS d'une intervention.")
    @EmitUpdateEvent(RESOURCE_REQUEST_CREATE)
    public ResponseEntity addResource(@PathVariable @Parameter(description = "L'identifiant de l'intervention") Long intervId,
                                      @RequestBody @Parameter(description = "Le moyen à ajouter") @Valid ResourceDTO resource,
                                      UriComponentsBuilder builder) {
        Resource newer = resourceMapper.reverseMap(resource);
        Resource created = service.addResource(intervId, newer);
        URI resourceUri = builder.path(ENDPOINT + "/{id}/resources/{resId}")
            .buildAndExpand(intervId, created.getId())
            .toUri();

        return ResponseEntity.created(resourceUri).build();
    }

    /**
     * Route permettant de créer une demande de moyens en masse pour une intervention.
     * Émet une notification de mise à jour de type <code>RESOURCE_REQUEST_CREATE</code> si l'opération a réussi.
     * Si la création d'un seul moyen échoue, la création de l'ensemble des moyens est annulée.
     *
     * @param intervId  L'id de l'intervention
     * @param resources Les moyens à demander
     * @return La réponse de l'API
     */
    @PostMapping("/{intervId}/resources/bulk")
    @Operation(
        description = "**Création** d'une demande de *moyens* en masse par le COS d'une intervention.<br/>" +
            "*Note : une erreur dans la transaction entraîne l'annulation de la demande pour tous les moyens.*",
        responses = {
            @ApiResponse(responseCode = "201", description = "Tous les moyens ont été ajoutés avec succès à l'intervention."),
            @ApiResponse(responseCode = "500", description = "Un moyen n'a pas pu être créé, la demande pour tous les moyens est annulée")
        }
    )
    @EmitUpdateEvent(RESOURCE_REQUEST_CREATE)
    public ResponseEntity bulkAddResources(@PathVariable @Parameter(description = "L'identifiant de l'intervention") Long intervId,
                                           @RequestBody @Parameter(description = "Les moyens à ajouter") @Valid List<ResourceDTO> resources) {
        List<Resource> newer = resourceMapper.reverseMapList(resources);
        service.bulkAddResources(intervId, newer);

        return ResponseEntity.status(HttpStatus.CREATED).build();
    }

    /**
     * Route permettant d'accepter un moyen demandé par le COS pour une intervention (uniquement CODIS).
     * Émet une notification de mise à jour de type <code>RESOURCE_REQUEST_ACCEPT</code> si l'opération a réussi.
     *
     * @param intervId    L'id de l'intervention
     * @param resId       L'id du moyen
     * @param attribution Un objet contenant les informations nécessaires à l'attribution du moyen, c'est-à-dire son nom
     * @return La réponse de l'API
     */
    @PatchMapping("/{intervId}/resources/{resId}/accept")
    @Operation(description = "**Accepte** une demande de *moyen*")
    @EmitUpdateEvent(RESOURCE_REQUEST_ACCEPT)
    public ResponseEntity validateResource(@PathVariable @Parameter(description = "L'identifiant de l'intervention") Long intervId,
                                           @PathVariable @Parameter(description = "L'identifiant du moyen") Long resId,
                                           @RequestBody @Parameter(description = "Les informations pour l'attribution du moyen")
                                           @Valid ResourceAttributionDTO attribution) {
        if (!roleService.isFromCodis())
            throw new UnauthorizedException(REQUIRE_CODIS);

        service.acceptResource(intervId, resId, attribution.name);
        return ResponseEntity.noContent().build();
    }

    /**
     * Route permettant de refuser un moyen demandé par le COS pour une intervention (uniquement CODIS).
     * Émet une notification de mise à jour de type <code>RESOURCE_REQUEST_REJECT</code> si l'opération a réussi.
     *
     * @param intervId L'id de l'intervention
     * @param resId    L'id du moyen
     * @return La réponse de l'API
     */
    @PatchMapping("/{intervId}/resources/{resId}/reject")
    @Operation(description = "**Refuse** (CODIS) ou **annule** (COS) une demande de *moyen*.")
    @EmitUpdateEvent(RESOURCE_REQUEST_REJECT)
    public ResponseEntity rejectResource(@PathVariable @Parameter(description = "L'identifiant de l'intervention") Long intervId,
                                         @PathVariable @Parameter(description = "L'identifiant du moyen") Long resId) {
        if (roleService.isFromCodis()) {
            service.rejectResource(intervId, resId);
        } else {
            service.cancelResource(intervId, resId);
        }

        return ResponseEntity.noContent().build();
    }

    /**
     * Route permettant de libérer un moyen d'une intervention.
     * Émet une notification de mise à jour de type <code>RESOURCE_RELEASE</code> si l'opération a réussi.
     *
     * @param intervId L'id de l'intervention
     * @param resId    L'id du moyen
     * @return La réponse de l'API
     */
    @PatchMapping("/{intervId}/resources/{resId}/release")
    @Operation(description = "Libération d'un *moyen*")
    @EmitUpdateEvent(RESOURCE_RELEASE)
    public ResponseEntity releaseResource(@PathVariable @Parameter(description = "L'identifiant de l'intervention") Long intervId,
                                          @PathVariable @Parameter(description = "L'identifiant du moyen") Long resId) {
        service.releaseResource(intervId, resId);
        return ResponseEntity.noContent().build();
    }

    /**
     * Route permettant de déplacer un moyen d'une intervention.
     * Émet une notification de mise à jour de type <code>RESOURCE_MOVE</code> si l'opération a réussi.
     *
     * @param intervId    L'id de l'intervention
     * @param resId       L'id du moyen
     * @param coordinates Les nouvelles coordonnées du moyen
     * @return La réponse de l'API
     */
    @PatchMapping("/{intervId}/resources/{resId}/move")
    @Operation(description = "Déplace un moyen sur site")
    @EmitUpdateEvent(RESOURCE_MOVE)
    public ResponseEntity moveResource(@PathVariable @Parameter(description = "L'identifiant de l'intervention") Long intervId,
                                       @PathVariable @Parameter(description = "L'identifiant du moyen") Long resId,
                                       @RequestBody @Parameter(description = "Les nouvelles coordonnées") CoordinateDTO coordinates) {
        service.moveResource(intervId, resId, coordinates.latitude, coordinates.longitude);
        return ResponseEntity.noContent().build();
    }

    /**
     * Route permettant de déplacer un point d'intérêt d'une intervention.
     * Émet une notification de mise à jour de type <code>POI_UPDATE</code> si l'opération a réussi.
     *
     * @param intervId    L'id de l'intervention
     * @param poiId       L'id du point d'intérêt
     * @param coordinates Les nouvelles coordonnées du point d'intérêt
     * @return La réponse de l'API
     */
    @PatchMapping("/{intervId}/pointsOfInterest/{poiId}/move")
    @Operation(description = "Déplace un point d'intérêt sur la carte")
    @EmitUpdateEvent(POI_UPDATE)
    public ResponseEntity movePointOfInterest(@PathVariable @Parameter(description = "L'identifiant de l'intervention") Long intervId,
                                              @PathVariable @Parameter(description = "L'identifiant du point d'intérêt") Long poiId,
                                              @RequestBody @Parameter(description = "Les nouvelles coordonnées") CoordinateDTO coordinates) {
        service.movePointOfInterest(intervId, poiId, coordinates.latitude, coordinates.longitude);
        return ResponseEntity.noContent().build();
    }

    /**
     * Route permettant de marquer un moyen comme étant arrivé sur une intervention / à son nouvel emplacement.
     * Émet une notification de mise à jour de type <code>RESOURCE_ARRIVE</code> si l'opération a réussi.
     *
     * @param intervId L'id de l'intervention
     * @param resId    L'id du moyen
     * @return La réponse de l'API
     */
    @PatchMapping("/{intervId}/resources/{resId}/arrive")
    @Operation(description = "Marque un moyen comme arrivé sur site/au nouvel emplacement (macro/micro)")
    @EmitUpdateEvent(RESOURCE_ARRIVE)
    public ResponseEntity setResourceArrived(@PathVariable @Parameter(description = "L'identifiant de l'intervention") Long intervId,
                                             @PathVariable @Parameter(description = "L'identifiant du moyen") Long resId) {
        service.setResourceArrived(intervId, resId);
        return ResponseEntity.noContent().build();
    }

    /**
     * Route permettant de renvoyer un moyen au CRM.
     * Émet une notification de mise à jour de type <code>RESOURCE_MOVE</code> si l'opération a réussi.
     *
     * @param intervId L'id de l'intervention
     * @param resId    L'id du moyen
     * @return La réponse de l'API
     */
    @PatchMapping("/{intervId}/resources/{resId}/crm")
    @Operation(description = "Le moyen retourne au CRM")
    @EmitUpdateEvent(RESOURCE_MOVE)
    public ResponseEntity moveResourceToCrm(@PathVariable @Parameter(description = "L'identifiant de l'intervention") Long intervId,
                                            @PathVariable @Parameter(description = "L'identifiant du moyen") Long resId) {
        service.moveResourceToCrm(intervId, resId);
        return ResponseEntity.noContent().build();
    }

    /**
     * Route permettant de changer le rôle d'un moyen.
     * Émet une notification de mise à jour de type <code>RESOURCE_UPDATE</code> si l'opération a réussi.
     *
     * @param intervId   L'id de l'intervention
     * @param resId      L'id du moyen
     * @param roleChange Un objet contenant le nouveau rôle à attribuer
     * @return La réponse de l'API
     */
    @PatchMapping("/{intervId}/resources/{resId}/role")
    @Operation(description = "Changer le rôle du moyen")
    @EmitUpdateEvent(RESOURCE_UPDATE)
    public ResponseEntity changeResourceRole(@PathVariable @Parameter(description = "L'identifiant de l'intervention") Long intervId,
                                             @PathVariable @Parameter(description = "L'identifiant du moyen") Long resId,
                                             @RequestBody @Parameter(description = "Un objet contenant le nouveau rôle") @Valid
                                                 RoleChangeDTO roleChange) {
        service.changeResourceRole(intervId, resId, roleChange.role);
        return ResponseEntity.noContent().build();
    }
}
