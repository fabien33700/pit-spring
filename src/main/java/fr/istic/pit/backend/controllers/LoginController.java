package fr.istic.pit.backend.controllers;

import fr.istic.pit.backend.model.dto.UserDTO;
import fr.istic.pit.backend.services.LoginService;
import fr.istic.pit.backend.services.mappers.UserMapper;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import static fr.istic.pit.backend.controllers.LoginController.ENDPOINT;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

/**
 * Contrôleur pour l'authentification de l'utilisateur.
 */
@RestController
@RequestMapping(ENDPOINT)
@Tag(name = ENDPOINT, description = "Le point de terminaison pour la connexion à l'application")
public class LoginController {

    /**
     * URI de base du contrôleur
     */
    public static final String ENDPOINT = "/login";

    /**
     * Service d'authentification
     */
    private final LoginService loginService;

    /**
     * Mapper d'utilisateur
     */
    private final UserMapper userMapper;

    /**
     * Constructeur du contrôleur
     *
     * @param loginService Service d'authentification
     * @param userMapper   Mapper d'utilisateur
     */
    public LoginController(LoginService loginService, UserMapper userMapper) {
        this.loginService = loginService;
        this.userMapper = userMapper;
    }

    /**
     * Gestion de connexion de l'application
     *
     * @param user les informations sur l'utilisateur reçu depuis la tablette
     * @return true si les informations sont OK, false sinon
     */
    @PostMapping(produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<String> login(@RequestBody UserDTO user) {
        boolean successful = loginService.login(userMapper.reverseMap(user));
        int status = successful ? 200 : 401;
        return ResponseEntity
            .status(status)
            .body("{}");
    }
}
