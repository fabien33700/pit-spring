create table mission
(
    id               serial
        constraint mission_pkey primary key,
    current_point_id bigint,
    intervention_id  bigint
        constraint fklv6de89ikv0qrff0y1qlndkve
            references intervention,
    mission_loop     boolean not null default true,
    terminated       boolean not null default false
);


create table drone_point
(
    id           serial
        constraint drone_point_pkey primary key,
    altitude     double precision,
    latitude     double precision,
    longitude    double precision,
    order_number integer,
    mission_id   bigint
        constraint fk1ecj193l9iobt4lgsn8suhk1p
            references mission
);

alter table intervention
    add column drone_availability varchar(255) default 'NONE',
    add column current_mission_id bigint
        constraint fku2gjlavyeiilc1wf5idmcq7oa
            references mission;