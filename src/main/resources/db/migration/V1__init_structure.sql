create table intervention
(
    id            serial
        constraint intervention_pkey
            primary key,
    active        boolean not null,
    end_time      timestamp,
    informations  varchar(255),
    latitude      double precision,
    longitude     double precision,
    name          varchar(255),
    sinister_code varchar(255),
    start_time    timestamp
);

create table map_points
(
    map_point_type  varchar(31) not null,
    id              serial      not null
        constraint map_points_pkey
            primary key,
    active          boolean     not null,
    latitude        double precision,
    longitude       double precision,
    picto_type      varchar(255),
    role            integer,
    arrival_time    timestamp,
    macro_state     varchar(255),
    micro_state     varchar(255),
    name            varchar(255),
    release_time    timestamp,
    request_time    timestamp,
    starting_time   timestamp,
    intervention_id bigint
        constraint fk2in5qu9x4o7xsgrum6bpdna4w
            references intervention
);

create table users
(
    id       serial
        constraint users_pkey
            primary key,
    login    varchar(255),
    password varchar(255)
);

