<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 https://maven.apache.org/xsd/maven-4.0.0.xsd">
	<modelVersion>4.0.0</modelVersion>
	<parent>
		<groupId>org.springframework.boot</groupId>
		<artifactId>spring-boot-starter-parent</artifactId>
		<version>2.2.5.RELEASE</version>
		<relativePath/> <!-- lookup parent from repository -->
	</parent>
	<groupId>fr.istic.pit</groupId>
	<artifactId>pit-spring</artifactId>
	<version>0.0.1-SNAPSHOT</version>
	<name>pit-spring</name>
	<description>PIT backend project with Spring Boot 2</description>

	<properties>
		<java.version>1.8</java.version>
        <project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
        <sonar.projectKey>pit_members_pit-spring</sonar.projectKey>
        <sonar.organization>pit-members</sonar.organization>
	</properties>

	<dependencies>
        <!-- Dépendances Spring Boot -->
		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-starter-data-jpa</artifactId>
		</dependency>
		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-starter-web</artifactId>
		</dependency>
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-security</artifactId>
            <version>2.2.4.RELEASE</version>
        </dependency>
        <dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-devtools</artifactId>
			<scope>runtime</scope>
			<optional>true</optional>
		</dependency>


        <!-- Spring API Doc -->
        <dependency>
            <groupId>org.springdoc</groupId>
            <artifactId>springdoc-openapi-ui</artifactId>
            <version>1.2.32</version>
        </dependency>

		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-starter-test</artifactId>
			<scope>test</scope>
			<exclusions>
				<exclusion>
					<groupId>org.junit.vintage</groupId>
					<artifactId>junit-vintage-engine</artifactId>
				</exclusion>
			</exclusions>
		</dependency>

        <!-- Source de données -->
		<dependency>
			<groupId>org.postgresql</groupId>
			<artifactId>postgresql</artifactId>
			<scope>runtime</scope>
		</dependency>

        <!-- Flyway -->
        <dependency>
            <groupId>org.flywaydb</groupId>
            <artifactId>flyway-core</artifactId>
            <version>6.0.7</version>
        </dependency>

        <!-- MQTT -->
        <dependency>
            <groupId>org.eclipse.paho</groupId>
            <artifactId>org.eclipse.paho.client.mqttv3</artifactId>
            <version>1.2.2</version>
        </dependency>

        <!-- ci sonarqube -->
        <dependency>
            <groupId>org.sonarsource.scanner.maven</groupId>
            <artifactId>sonar-maven-plugin</artifactId>
            <version>3.7.0.1746</version>
        </dependency>

        <!-- Lecture de fichier -->
        <dependency>
            <groupId>commons-io</groupId>
            <artifactId>commons-io</artifactId>
            <version>2.6</version>
        </dependency>

        <!-- Bug fix : génération de la Javadoc avec le JDK8 -->
        <dependency>
            <groupId>javax.interceptor</groupId>
            <artifactId>javax.interceptor-api</artifactId>
            <version>1.2.2</version>
        </dependency>
	</dependencies>



    <profiles>
        <!-- Développement local -->
        <profile>
            <id>local</id>
            <properties>
                <jdbc.url>jdbc:postgresql://localhost:5432/pitproject</jdbc.url>
                <jdbc.username>pitproject</jdbc.username>
                <jdbc.password>pitproject</jdbc.password>
                <mqtt.broker.url>tcp://localhost:1883</mqtt.broker.url>
            </properties>
            <activation>
                <activeByDefault>true</activeByDefault>
            </activation>
        </profile>
        <!-- Conteneur Docker -->
        <profile>
            <id>container</id>
            <properties>
                <!--suppress UnresolvedMavenProperty -->
                <jdbc.url>jdbc:postgresql://pit-spring_db:5432/pitproject</jdbc.url>
                <jdbc.username>pitproject</jdbc.username>
                <jdbc.password>pitproject</jdbc.password>
                <mqtt.broker.url>tcp://pit-spring_mqtt:1883</mqtt.broker.url>
            </properties>
        </profile>
        <!-- Pipelines (Shared Runners) -->
        <profile>
            <id>test</id>
            <properties>
                <!--suppress UnresolvedMavenProperty -->
                <jdbc.url>jdbc:postgresql://${env.SERVER_URL_EXTERN}:${env.SERVER_PORT_DATABASE_EXTERN}/pitproject</jdbc.url>
                <jdbc.username>pitproject</jdbc.username>
                <jdbc.password>pitproject</jdbc.password>
                <!--suppress UnresolvedMavenProperty -->
                <mqtt.broker.url>tcp://${env.SERVER_URL_EXTERN}:${env.SERVER_PORT_MQTT_EXTERN}</mqtt.broker.url>
            </properties>
        </profile>
    </profiles>

	<build>
		<plugins>
			<plugin>
				<groupId>org.springframework.boot</groupId>
				<artifactId>spring-boot-maven-plugin</artifactId>
			</plugin>
		</plugins>
        <resources>
            <resource>
                <directory>src/main/resources</directory>
                <filtering>true</filtering>
            </resource>
        </resources>
	</build>

</project>
