#!/bin/bash

[[ -z "$PORT_DB" ]] && export PORT_DB=5432
[[ -z "$PORT_SRV" ]] && export PORT_SRV=8080
[[ -z "$PORT_MQTT" ]] && export PORT_MQTT=1883

docker-compose up -d --build
